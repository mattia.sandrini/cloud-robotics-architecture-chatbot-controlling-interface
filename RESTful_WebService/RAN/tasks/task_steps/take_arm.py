from .task_step import TaskStep

__author__ = 'Mattia Sandrini'

class TakeArm(TaskStep):

    def __init__(self, name="take_arm", parent_task=None):
        super(TakeArm, self).__init__(name, parent_task)

    def execute(self):
        if not self._thread_stop_event.isSet():
            robot = self.parent_task.robot
            robot.take_arm()

    def clone(self):
        return TakeArm(name=self.name, parent_task=self.parent_task)

    def serialize(self):
        return {
            'name': self.name
        }
