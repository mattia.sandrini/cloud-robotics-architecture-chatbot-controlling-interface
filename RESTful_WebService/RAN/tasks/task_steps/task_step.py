from time import sleep
from threading import Thread, Event

__author__ = 'Mattia Sandrini'

class TaskStep(Thread):

    name = None
    parent_task = None

    def __init__(self, name="nop", parent_task=None):
        self.name = name
        self.parent_task = parent_task   # Task whose this step is part of

        self._thread_stop_event = Event()
        super(TaskStep, self).__init__()

    def execute(self):
        if not self._thread_stop_event.isSet():
            pass

    def clone(self):
        return TaskStep(name=self.name, parent_task=self.parent_task)

    def run(self):
        self.execute()

    def force_stop(self):
        self._thread_stop_event.set()

    def serialize(self):
        return {
            'name': self.name
        }
