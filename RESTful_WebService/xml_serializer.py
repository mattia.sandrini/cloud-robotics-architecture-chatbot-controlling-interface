# author: Mattia Sandrini 

##
# I moduli xml_serializer e xml_deserializer sono stati progettati per serializzare e deserializzare 
# su/da file XML il contenuto di oggetti, istanze di classi generiche, organizzati in una lista.
# La struttura degli oggetti è gerarchica, ovvero, un oggetto può contenere un'altra lista di oggetti
# non necessariamente omogenei, e così via senza limiti di profondità.
# Non ci sono quasi limiti alla complessità che si può dare alle classi,
# possono contenere attributi di tipo builtin (int, float, bool, string, dict, list),
# ma non attributi oggetti istanze di classi!
# Gli attributi lista, possono contenere dati di tipo builtin (tranne liste), e oggetti,
# questi oggetti potrenno contenere a loro volta liste di oggetti, e così via...
# Le classi possono essere estese con nuovi attributi, o anche ereditare / venire ereditate da altre classi.
#
# Limiti: 
#  - Un oggetto non può avere come attributo un altro oggetto che non sia di tipo builtin (genera errore)
#  - Un attributo lista di un oggetto non può a sua volta contenere una lista(la seconda lista non viene registrata e genera errore)


from xml.etree.ElementTree import Element, SubElement, Comment, tostring
import xml.dom.minidom
import logging


# ElementTree.Element.tag     # Fornisce il nome del tag oggetto di Element
# ElementTree.Element.attrib  # Fornisce un dizionario di attributi associati al tag oggetto di Element
# ElementTree.Element.text    # Fornisce il dato contenuto nel tag oggetto di Element


## Fornisce un dizionario con chiavi pari al nome degli attributi dell'oggetto generico obj, e i valori corrispettivi per quell'istanza
#  @return  {'attr1': value1, 'attr2': value2, ...}  per ogni attributo(che non sia funzione) della classe
def get_attributes(obj):
    result = {}
    for attr in dir(obj):               # Per ogni attributo presente nella classe dell'oggetto obj
        if not attr.startswith("__") and not attr.startswith("_"):   # Ignoro gli attributi speciali (come __init__, ecc...)
            value = getattr(obj, attr)  # Ottengo il valore dell'attributo attualmente considerato
            if not callable(value):     # Ignoro anche gli attribui che sono funzioni
                result[attr] = value    # Aggiungo al dizionario la nuova coppia (chiave: valore)
    return result   


##
# Funzione ricorsiva e generica, per costruire alberi XML partendo da oggetti istanze di classi contenti attributi atomici e liste di altri oggetti.
# @param root Elemento radice dell'albero, o del sotto-albero, al quale si vuole appendere il sotto-albero generato dalla lista in ingresso
# @param obj_list Lista di oggetti (non necessariamente omogenei) di cui viene generato il sotto-albero che sarà appeso alla radice root specificata
# @param id_attrs si specificano gli identificatori che verranno posti come attributo del tag radice di una lista, in modo da agevolare le ricerche
def generate_list_tree(root, obj_list=[], id_attrs=[]):
    for item in obj_list:  
        
        if isinstance(item, (int, float, complex, bool, str, dict)):  # Se nella lista è presente un tipo di dato builtin
            type_name = type(item).__name__.lower()                   # Ottengo il nome del tipo di dato
            item_tag = SubElement(root, type_name)                    # Per ogni elemento della lista creo un proprio tag
            item_tag.set('type', type_name)                           # Specifico come attributo del tag il tipo di dato
            item_tag.text = str(item)                                 # Scrivo il valore
            continue

        item_attrs = get_attributes(item)        # Ottengo il dizionario di coppie attributo:valore, conetenute nell'oggetto attualemente considerato
        name_item = type(item).__name__.lower()  # ottengo il nome della classe istanziata dall'oggetto, tutto in minuscolo
        item_tag = SubElement(root, name_item)  
        
        item_tag.set('class', str(item.__class__).split("'")[1]) # Come attributo 'class' scrivo la classe a cui appartengono gli oggetti che compongono la lista

        for key in item_attrs:     
            if key in id_attrs:                         # Se tra gli attributi dell'oggetto considerato nella lista, ne ho trovato uno che è indicato come attributo identificatore
                item_tag.set(key, str(item_attrs[key])) # allora al tag di quell'oggetto gli associo l'attributo contenente il valore dell'identificatore
        
        for key in item_attrs:       # key contiene, di volta in volta, il nome degli attributi di un oggetto building
            attr_type = type(item_attrs[key]).__name__  # Ottengo il tipo di dato dell'attributo considerato
            if attr_type != 'list':   # Se non sto considerando un attributo lista (e quindi è un attributo atomico)
                tag = SubElement(item_tag, key)     # Creo un elemento figlio del tag building e gli do il nome dell'attributo stesso
                tag.set('type', attr_type)
                tag.text = str(item_attrs[key])     # All'interno di questo tag scrivo il valore dell'attributo
            else:
                sub_root = SubElement(item_tag, key)    # Elemento sotto-radice che conterrà tutti gli elementi della sotto-lista, e sarà aggiunto al tag attualmente aperto
                sub_root.set('type', attr_type)
                
                
                #print(isinstance(item_attrs[key][0], (int, float, complex)))
                
                generate_list_tree(sub_root, item_attrs[key], id_attrs)  # Procedo iterativamente
    return


##
# Formatta in una stringa indentata l'albero generato, poi lo scrivo in nel file specificato
# @param root Elemento radice dell'albero XML che si vuole scrivere su file
# @param file Nome del file sul quale deve essere scritto l'albero XML
def write_xml(root, file):
    ugly_xml = xml.dom.minidom.parseString(tostring(root))
    pretty_xml_as_string = ugly_xml.toprettyxml()   # Trasforma la stringa di tag piatta, in una stringa ben indentata
    f = open(file, 'w')
    f.write(pretty_xml_as_string)


## 
# Data una lista di oggetti di tipo Building, ne genera l'albero XML corrispondente,
# lo formatta in una stringa indentata e lo scrive sul file specificato.
# @param buildings Lista di oggetti istanza della classe Building 
# @param file Nome del file in cui verrà scritto l'albero XML generato 
def serialize(obj_list=[], root_name='root', id_attrs=[], file = "out.xml"):
    try:
        root = Element(root_name)  # Elemento radice
        root.append(Comment('File generated by the function xml_serializer.serialize()'))
        if len(obj_list) == 0:      # Se non ci sono elementi che andaranno a comporre l'albero
            pass                    # si potrà dare una segnalazione, scrivere un log, ...
        generate_list_tree(root, obj_list, id_attrs)  # Richiamo il metodo ricorsivo che costruisce l'albero, ed i sottoalberi costituiti da liste di oggetti generici
        write_xml(root, file)       # Scrivo l'albero su file
        return True
    except Exception as e:
        logging.error("XML Deserialization: " + str(e))
        return False




# Test
"""
from WSN import building, room

b1 = building.Building()
b2 = building.Building()
r1 = room.Room()

serialize([b1, b2], 'buldings', ['building_id', 'room_id', 'node_id', 'sensor_id', 'actuator_id', 'variable_id'], 'test.xml')

#serialize([])   # Il file xml risulterà: <?xml version="1.0" ?> <buildings></buildings>
#serialize([b1, r1, b2], 'buldings', ['building_id', 'room_id']) # Funziona anche se la lista non è omogenea, ma questo non interessa, non è il nostro caso.
"""





""" Formato del file xml generato:
<?xml version="1.0" ?>
<buildings>
    <!--File contenente tutti gli oggetti Building, e le relative informazioni.--><building building_id="1" class="WSN.building.Building">
        <building_year type="int">1995</building_year>
        <energy_class type="str">A++</energy_class>
        ...
        <rooms type="list">
            <room class="WSN.room.Room" room_id="101">
                <identifier type="str">M1</identifier>
                <room_id type="int">101</room_id>
                <ip_gateway type="str">192.168.1.100</ip_gateway>
                ...
            </room>
            <room class="WSN.room.Room" room_id="102">
                <identifier type="str">MLab</identifier>
                <room_id type="int">102</room_id>
                <ip_gateway type="str">192.168.1.100</ip_gateway>
                ...
            </room>
            <room class="WSN.room.Room" room_id="103">
                <identifier type="str">MTA</identifier>
                <room_id type="int">103</room_id>
                <ip_gateway type="str">192.168.1.100</ip_gateway>
                ...
            </room>
        </rooms>
        <building_id type="int">1</building_id>
        ...
    </building>
    <building building_id="2" class="WSN.building.Building">
        <building_year type="int">1995</building_year>
        <energy_class type="str">A++</energy_class>
        ...
        <rooms type="list">
            <room class="WSN.room.Room" room_id="104">
                <identifier type="str">M1</identifier>
                <room_id type="int">104</room_id>
                <ip_gateway type="str">192.168.1.100</ip_gateway>
                ...
            </room>
            <room class="WSN.room.Room" room_id="105">
                <identifier type="str">MLab</identifier>
                <room_id type="int">105</room_id>
                <ip_gateway type="str">192.168.1.100</ip_gateway>
                ...
            </room>
            <room class="WSN.room.Room" room_id="106">
                <identifier type="str">MTA</identifier>
                <room_id type="int">106</room_id>
                <ip_gateway type="str">192.168.1.100</ip_gateway>
                ...
            </room>
        </rooms>
        <building_id type="int">2</building_id>
        ...
    </building>
</buildings>
"""
