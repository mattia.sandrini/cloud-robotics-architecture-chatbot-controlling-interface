# author: Emilio Baresi

import json
import functions
import telegram as tel
import time

context= ""

#Inizialization of the Telegram listener
try:
    text, chat, message_id = tel.get_last_chat_id_and_text(tel.get_updates())
except Exception:
    text, chat, message_id = (None,None,None)
last_textchat = (text, chat, message_id)
last_message_id = last_textchat[2]
print(last_message_id)


#Telegram listener, every 0.5 second it makes a Telegram API call to check if there are new messages
while True:
    sem = True
    while sem:
        time.sleep(0.5)
        if tel.any_updates():
            text, chat, message_id = tel.get_last_chat_id_and_text(tel.get_updates())
            if message_id != last_message_id:
                sem = False
    msg = text

    #The message is sent to the IBM services to be analyzed
    answer = functions.sendMessageToConversation(msg, context)
    answerJSON = answer.json()
    context = answerJSON['context']
    node_visited = answerJSON['output']['nodes_visited']


    if len(answerJSON['intents'])>0:
        intent = answerJSON['intents'][0]['intent']
    else:
        intent = ""
    if len(answerJSON['entities'])>0:
        entity = answerJSON['entities'][0]
    else:
        entity = ""

    try:
        output = ""
        for i in range(0, len(answerJSON['output']['text'])):
            output += answerJSON['output']['text'][i] + "\n"
    except:
        output = ""

    #After the intent has been understood that intent has to be handled
    context, ret = functions.handleIntent(intent, entity, output, context)

    context = json.dumps(context)

    #Sending the answer to the user through Telegram
    tel.send_message(ret, chat)
    last_textchat = (text, chat, message_id)
    last_message_id = last_textchat[2]

