# author: Emilio Baresi

import json
import requests

#Token for the Telegram API
TOKEN = "671301063:AAHZbhk8aNe94EWucL5JGZn3OKpNoWiczRI"
URL = "https://api.telegram.org/bot"+TOKEN+"/"


def get_url(url):
    response = requests.get(url)
    content = response.content.decode("utf8")
    return content


def get_json_from_url(url):
    content = get_url(url)
    js = json.loads(content)
    return js


def get_updates():
    url = URL + "getUpdates"
    js = get_json_from_url(url)
    return js

def any_updates():  # Return True if there are updates, False otherwise
    updates = get_updates()
    return len(updates["result"]) > 0

def get_last_chat_id_and_text(updates):
    num_updates = len(updates["result"])
    if num_updates == 0:
        return None
    else:
        last_update = num_updates - 1
        text = updates["result"][last_update]["message"]["text"]
        chat_id = updates["result"][last_update]["message"]["chat"]["id"]
        message_id = updates["result"][last_update]["message"]["message_id"]
        return (text, chat_id, message_id)


def send_message(text, chat_id):
    url = URL + "sendMessage?text={}&chat_id={}".format(text, chat_id)
    get_url(url)
