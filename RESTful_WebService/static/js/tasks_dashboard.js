TASKS_QUEUE_ACTION_EXECUTE_ALL = 'execute_all';
TASKS_QUEUE_ACTION_EXECUTE_NEXT = 'execute_next';
TASKS_QUEUE_ACTION_ENQUEUE_TASK = 'enqueue_task';
TASKS_QUEUE_ACTION_HALT = 'halt';

INTERPOLATION_TYPES = {'P': "PTP", 'L': "Linear", 'A': "Arc", 'S': "Spline"};


function update_select_task_names()
{
    var uri = "http://{0}/robots/{1}/tasks/executables".format(host, robot_id);
    $.get(uri, function(data, status) {
        result = JSON.parse(data);
        if (status == "success") {
            if (result['committed']) {
                var options = "";
                var tasks = result['executable_tasks'];
                for (i=0; i<tasks.length; i++)
                    options += "<option value='{0}'>{0}</option>".format(tasks[i]['task_name']);

                $("#task_name").html(options);
            }
        }
        if (result['committed'] == false) {

        }
    });
}


function create_progress_bar(id, progress_value, measured_duration, halted) {
    var progress_value = Math.round(parseFloat(progress_value)*100).toString();
    var animation = (progress_value == "100" || halted || measured_duration < 0) ? '' : 'progress-bar-animated';
    var title_text = "";
    if (halted)
        title_text = "title='Halted'";
    else if (measured_duration >= 0)
        title_text = "title='Duration: {0} s'".format(measured_duration);
    return "<div id='{0}' class='progress-bar progress-bar-striped {2}' style='width:{1}%' {3}>{1}%</div>".format(id, progress_value, animation, title_text);
}

function update_progress_bar(id, progress_value) {
    progress_value = Math.round(parseFloat(progress_value)*100).toString();
    progress_percent = '{0}%'.format(progress_value);
    $("#{0}".format(id)).css('width', progress_percent);
    $("#{0}".format(id)).html(progress_percent);

    if (progress_value == parseInt(100)) {
        setTimeout(function(){
            $("#{0}".format(id)).removeClass('progress-bar-animated');
        }, 200);
    }
}

function update_progress_bar_measured_durtion(id, measured_duration) {
    $("#{0}".format(id)).attr('title', "Duration: {0} s".format(measured_duration));
}

function update_tasks_queue(tasks)
{
    $("#tasks_queue").html("");  // Reset

    for (var i=0; i<tasks.length; i++) {
        var duration = Math.round(parseFloat(tasks[i]['measured_duration']) * 1000) / 1000.; // Remove the last 3 decimal digits
        var text = "<div class='row'>" +
                   "   <div class='col-lg-4 pr-2'>" + tasks[i]['task_id'] + "</div>" +
                   "   <div class='progress col-lg-8 p-0'>" +
                   create_progress_bar(tasks[i]['task_id'], tasks[i]['progress'], duration, tasks[i]['status_code']==3) +  // Task.STATUS_STATE_HALTED = 3
                   "   </div>" +
                   "</div>";
        $("#tasks_queue").append(text);
    }
}

function init_tasks_queue()
{
    var uri = "http://{0}/robots/{1}/tasks/running_queue".format(host, robot_id);
    $.get(uri, function(data, status) {
        result = JSON.parse(data);
        if (status == "success") {
            if (result['committed']) {
                var tasks = result['tasks_running_queue'];
                update_tasks_queue(tasks);
            }
        }
        if (result['committed'] == false) {

        }
    });
}

function init_tasks_elements_action()
{
    $("#enqueue_task_btn").click(function() {
        robot_id = window.robot_id;
        var uri = "http://{0}/robots/{1}/tasks/running_queue".format(host, robot_id);
        var task_name = $("#task_name").val();  // From TaskName Selector
        $.post(uri, { 'action': TASKS_QUEUE_ACTION_ENQUEUE_TASK, 'task_name': task_name }, function(result, status) {
            result = JSON.parse(result);
            if (status == "success") {
                if (result['committed'] == true) {
                    var length = result['tasks_running_queue'].length;
                    show_success_alert("Task \"{0}\" enqueued. There are {1} tasks in the running queue.".format(result['tasks_running_queue'][length-1]['task_id'], length));
                    update_tasks_queue(result['tasks_running_queue']);
                }
            }
            if (result['committed'] == false)
                show_error_alert(result['error_msg'].replace('\n', '<br />'));
                //alert("Error in adding the task to the queue!\nResult: " + result['error_msg']);
        });
    });


    $("#delete_task_btn").click(function() {
        var robot_id = window.robot_id;
        var task_name = $("#task_name").val();
        $.ajax({
            url: 'http://{0}/robots/{1}/tasks/executables/{2}'.format(host, robot_id).format(host, robot_id, task_name),
            method: 'DELETE',
            contentType: 'application/json',
            success: function(result) {
                result = JSON.parse(result);
                if (result['committed'] == true) {
                    show_success_alert("Task \"{0}\" deleted.".format(task_name));
                    update_select_task_names();
                }
                else
                    show_error_alert(result['error_msg'].replace('\n', '<br />'));
                    //alert("Error in deleting the task!\nError: " + result['error_msg']);
            },
            error: function(xhr, textStatus, errorThrow) {
                result = JSON.parse(xhr.responseText);
                if (result['committed'] == false)
                    show_error_alert(result['error_msg'].replace('\n', '<br />'));
                    //alert(errorThrow + ":\n\nError in deleting the task!\nError: " + result['error_msg']);
            }
        });
    });



    $("#execute_Q_btn").click(function() {
        robot_id = window.robot_id;
        var uri = "http://{0}/robots/{1}/tasks/running_queue".format(host, robot_id);
        var delay = 1;   // Delay of 1 second
        $.post(uri, { 'action': TASKS_QUEUE_ACTION_EXECUTE_ALL, 'delay': delay }, function(result, status) {
            result = JSON.parse(result);
            if (status == "success") {
                if (result['committed'] == true) {
                    show_success_alert("Starting to execute the tasks in the running queue.");
                    update_tasks_queue(result['tasks_running_queue']);
                }
            }
            if (result['committed'] == false)
                show_error_alert(result['error_msg'].replace('\n', '<br />'));
                //alert("Error in running the task queue!\nResult: " + result['error_msg']);
        });
    });

    $("#halt_btn").click(function() {
        robot_id = window.robot_id;
        var uri = "http://{0}/robots/{1}/tasks/running_queue".format(host, robot_id);

        $.post(uri, { 'action': TASKS_QUEUE_ACTION_HALT }, function(result, status) {
            result = JSON.parse(result);
            if (status == "success") {
                if (result['committed'] == true) {
                    show_success_alert("The running queue has been halted.");
                    update_tasks_queue(result['tasks_running_queue']);
                }
            }
            if (result['committed'] == false)
                show_error_alert(result['error_msg'].replace('\n', '<br />'));
                //alert("Error in halting the running task queue!\nResult: " + result['error_msg']);
        });
    });

    $("#clear_queue_btn").click(function() {
        robot_id = window.robot_id;
        $.ajax({
            url: 'http://{0}/robots/{1}/tasks/running_queue'.format(host, robot_id).format(host, robot_id),
            method: 'DELETE',
            contentType: 'application/json',
            success: function(result) {
                result = JSON.parse(result);
                if (result['committed'] == true) {
                    show_success_alert("The running queue is empty now.");
                    update_tasks_queue([]);
                }
                else
                    show_error_alert(result['error_msg'].replace('\n', '<br />'));
                    //alert("Error in deleting the tasks running queue!\nError: " + result['error_msg']);
            },
            error: function(xhr, textStatus, errorThrow) {
                result = JSON.parse(xhr.responseText);
                if (result['committed'] == false)
                    show_error_alert(result['error_msg'].replace('\n', '<br />'));
                    //alert(errorThrow + ":\n\nError in deleting the tasks running queue!\nError: " + result['error_msg']);
            }
        });
    });

    $("#define_task_form").submit(function(e) {
        e.preventDefault();
        var form = this;

        var task_name = $("#new_task_name").val();
        var task_type = $("#new_task_type").val();
        var task_repetitions = parseInt($("#new_task_repetitions").val());
        if (task_repetitions == null || isNaN(task_repetitions))
            task_repetitions = 1;
        var repetitions_delay = parseInt($("#new_task_delay").val());
        if (repetitions_delay == null || isNaN(repetitions_delay))
            repetitions_delay = 0;

        var data_payload = { 'type': task_type, 'task_repetitions': task_repetitions, 'repetitions_delay': repetitions_delay };

        if (task_type == "do_nothing") {
            data_payload['params'] = null
        }
        else if (task_type == "pick_and_place") {
            var points = Array();
            var i = 0;
            // Skip the first div (the one with labels) and the last one (the '+' button)
            $("#pick_and_place_task_definition").children("div").slice(1, -1).each(function(){
                points[i] = {'pick_approach': $(this).children(':nth-child(1)').val(),
                             'pick': $(this).children(':nth-child(2)').val(),
                             'place_approach': $(this).children(':nth-child(3)').val(),
                             'place': $(this).children(':nth-child(4)').val()};
                i++;
            });

            data_payload['params'] = JSON.stringify({'points': points});
        }
        else if (task_type == "custom") {
            var steps = Array();
            var i = 0;
            $("#custom_task_definition").children("div").slice(1, -1).each(function() {
                var step_type = $(this).children(':nth-child(1)').val();
                var value_elements = $(this).children(':nth-child(1)').next().children().not("label");
                switch (step_type) {
                    case "move_to":
                        steps[i] = {'step_type': step_type,
                                    'point_name': value_elements.eq(0).val(),
                                    'pass_type': value_elements.eq(1).val(),
                                    'interp': value_elements.eq(2).val() };
                    break;

                    case "wait":
                    case "set_speed": steps[i] = {'step_type': step_type, 'value': parseInt(value_elements.eq(0).val())}; break;

                    case "take_arm":
                    case "give_arm": steps[i] = {'step_type': step_type}; break;
                }
                i++;
            });

            data_payload['params'] = JSON.stringify({'steps': steps});
        }

        var check = true;
        if (check) {
            $.ajax({
                url: "http://{0}/robots/{1}/tasks/executables/{2}".format(host, robot_id, task_name),
                type: 'PUT',
                //contentType: 'application/json',
                data: data_payload,
                success: function(response) {
                    result = JSON.parse(response);
                    if (result['committed'] == true) {
                        show_success_alert("The new task \"{0}\" has been defined.".format(task_name));
                        //update_select_task_names();
                        location.reload();
                    }
                    else
                        show_error_alert(result['error_msg'].replace('\n', '<br />'));
                        //alert("Error in adding the new Task!\nResult: " + response['error_msg']);
                },
                error: function(xhr, textStatus, errorThrow) {
                    result = JSON.parse(xhr.responseText);
                    if (result['committed'] == false)
                        show_error_alert(result['error_msg'].replace('\n', '<br />'));
                        //alert(errorThrow + ":\n\nError in adding the new Task!\nResult: " + result['error_msg']);
                }
            });
        }
    });

    function generate_point_name_selector(class_col, class_offset) {
        var options = "";
        for (var i=0; i<100; i++)
            options += "<option value='P{0}'>P{0}</option>".format(i);

        return "<select class='form-control {0} {1}'>".format(class_offset, class_col) + options + "</select>";
    }

    function generate_pnp_points_group() {
        text = "<div class='form-group row'>" +
               generate_point_name_selector('col-lg-2', 'offset-lg-1') +
               generate_point_name_selector('col-lg-2', '') +
               generate_point_name_selector('col-lg-2', '') +
               generate_point_name_selector('col-lg-2', '') +
               "</div>";
        return text;
    }

    $("#new_task_pnp_add_points").click(function() {
        //var index = $(this).val();
        $(this).parent().before(generate_pnp_points_group());
        //$(this).val(index++);
    });


    function generate_pass_type_selector(class_col, class_offset) {
        var options = "<option value='@P'>@P</option>" +
                      "<option value='@E'>@E</option>";
        for (var i=0; i<10; i++)
            options += "<option value='@{0}'>@{0}</option>".format(i);

        return "<select class='form-control {0} {1}'>".format(class_offset, class_col) + options + "</select>";
    }


    function generate_custom_new_step(index) {
        text = "<div class='form-group row'>" +
               "    <select id='type_new_step_{0}' class='form-control offset-lg-1 col-lg-2'>".format(index) +
               "        <option value='take_arm'>Take Arm</option>" +
               "        <option value='give_arm'>Give Arm</option>" +
               "        <option value='wait'>Wait</option>" +
               "        <option value='set_speed'>Set Speed</option>" +
               "        <option value='move_to'>Move To</option>" +
               "    </select>" +
               "    <div id='params_new_step_{0}' class='form-group row col-lg-8'></div>".format(index) +
               "</div>";
        return text;
    }

    function init_last_step_actions() {
        $("#custom_task_definition").children("div").slice(-2, -1).each(function(){  // Take just the last insserted new_task_step

            $(this).children(':nth-child(1)').change(function() {
                var step_type = $(this).val();
                var text = "";

                switch (step_type) {
                    case "take_arm": break;
                    case "give_arm": break;

                    case "wait":
                        text += "<label class='col-form-label col-lg-5 text-right'>Value (ms): </label>" +
                                "<input type='text' value='0' class='form-control col-lg-3'>";
                        break;

                    case "set_speed":
                        text += "<label class='col-form-label col-lg-5 text-right'>Value (1&divide;100): </label>" +
                                "<input type='text' value='1' class='form-control col-lg-3'>";
                        break;

                    case "move_to":
                        text += "<label class='col-form-label col-lg-2 text-right'>Point: </label>" + generate_point_name_selector('col-lg-2', '') +
                                "<label class='col-form-label col-lg-2 text-right'>Passtype: </label>" + generate_pass_type_selector('col-lg-2', '') +
                                "<label class='col-form-label col-lg-2 text-right'>Interp: </label>" +
                                "<select class='form-control col-lg-2'>";
                        for (key in INTERPOLATION_TYPES) {
                            text += "<option value='{0}'>{1}</option>".format(key, INTERPOLATION_TYPES[key]);
                        }
                        text += "</select>";
                        break;
                }

                $(this).next().html(text);
            });
        });
    }


    $("#new_task_custom_add_step").click(function() {
        var index = $(this).val();
        $(this).parent().before(generate_custom_new_step());
        $(this).val(index++);

        init_last_step_actions();
    });






    $("#new_task_type").change(function() {
        var task_type = $(this).val();
        if (task_type == 'custom') {
            $("#custom_task_definition").show();
            $("#pick_and_place_task_definition").hide();
            $("#do_nothing_task_definition").hide();
        }
        else if (task_type == 'pick_and_place') {
            $("#custom_task_definition").hide();
            $("#pick_and_place_task_definition").show();
            $("#do_nothing_task_definition").hide();
        }
        if (task_type == 'do_nothing') {
            $("#custom_task_definition").hide();
            $("#pick_and_place_task_definition").hide();
            $("#do_nothing_task_definition").show();
        }
    });


    $('#new_task_repetitions').on('input', function(e) {
        n = parseInt($(this).val());
        if (!isNaN(n)) {
            n = (n < 0) ? 0 : n;
            if (n <= 1)
                $("#new_task_delay").attr('disabled', 'disabled');
            else
                $("#new_task_delay").removeAttr('disabled');
            $(this).val(n);
        }
    });
}


function init_robot_tasks_container()
{
    text = "<h4>Tasks</h4>" +
           "<div class='row pb-2'>" +
           //"    <div class='col-lg-12'>" +
           "        <label for='task_name' class='offset-lg-1 col-lg-2 col-form-label'>Task:</label>" +
           "        <select name='task_name' id='task_name' class='form-control col-lg-6'>" +
           "        </select>" +
           "    <div class='col-lg-3'><button id='enqueue_task_btn' class='btn btn-info btn-md w-100' type='button'>Enqueue</button></div>" +
           //"    </div>" +
           "</div>" +
           "<div class='row pb-3'>" +
           //"    <div class='offset-lg-4 col-lg-2'><button id='define_task_btn' class='btn btn-info btn-md w-100' type='button' data-target='#defineTaskModal' data-toggle='modal'>New</button></div>" +
           //"    <div class='col-lg-3'><button id='modify_task_btn' class='btn btn-info btn-md w-100' type='button' data-target='#modifyTaskModal' data-toggle='modal'>Modify</button></div>" +
           //"    <div class='col-lg-3'><button id='delete_task_btn' class='btn btn-info btn-md w-100' type='button'>Delete</button></div>" +
           "    <button id='define_task_btn' class='btn btn-info btn-md offset-lg-1 col-lg-2' type='button' data-target='#defineTaskModal' data-toggle='modal'>New</button>" +
           "    <button id='modify_task_btn' class='btn btn-info btn-md col-lg-3' type='button' data-target='#modifyTaskModal' data-toggle='modal'>Modify</button>" +
           "    <button id='delete_task_btn' class='btn btn-info btn-md col-lg-3' type='button'>Delete</button>" +
           "</div>" +
           "<h5>Tasks Queue</h5>" +
           "<div class='row pb-2'>" +
           "    <button id='execute_Q_btn' class='btn btn-info btn-sm col-lg-3 m-1' type='button'>Execute Q</button>" +
           "    <button id='halt_btn' class='btn btn-info btn-sm col-lg-3 m-1' type='button'>Halt</button>" +
           "    <button id='clear_queue_btn' class='btn btn-info btn-sm col-lg-3 m-1' type='button'>Clear</button>" +
           "</div>" +
           "<div class='row'>" +
           "    <div id='tasks_queue' class='col-lg-12'>" +
           "    </div>" +
           "</div>";

    $("#robot_tasks_container").html(text);

    update_select_task_names();
    init_tasks_queue();

    init_tasks_elements_action();
}


function init_tasks_dashboard()
{
    init_robot_tasks_container();

    //connect to the socket server.
    var socket = io.connect('http://' + document.domain + ':' + location.port + '/monitor');
    socket.on('progress_msg', function(msg) {
        update_progress_bar(msg.task_id, msg.progress);
    });

    socket.on('finish_task_msg', function(msg) {
        var duration = Math.round(parseFloat(msg.measured_duration) * 1000) / 1000.; // Remove the last 3 decimal digits
        update_progress_bar_measured_durtion(msg.task_id, duration);
        show_success_alert("Task \"{0}\" executed in {1} s.".format(msg.task_id, duration));
    });
}

