import time
import logging
from RAN.tasks.task import Task

__author__ = 'Mattia Sandrini'

class Robot:
    #TEMPORARY_POINT_P = "P99"  # Point used for modifying the robot's current position
    #TEMPORARY_POINT_J = "J99"  # Point used for modifying the robot's current position
    #TEMPORARY_POINT_T = "T99"  # Point used for modifying the robot's current position
    #TEMPORARY_POINT = { 'P': TEMPORARY_POINT_P, 'J': TEMPORARY_POINT_J, 'T': TEMPORARY_POINT_T }

    POINTS_PER_TYPE = 100   # Amount of points according to each type of point (as for Denso standard)
    DEFAULT_POINT_VALUES = { 'P': [0]*6 + [-1], 'J': [0]*8, 'T': [0]*9 + [-1] }   # Denso standard format


    ### Generic attributes
    robot_id = None           # String
    running_task = None       # Instance of the task the robot is currently running

    points = None             # dict: { 'P0': [...], ..., 'J0': [...], 'T0': [...], ...}
    points_synched = False    # True when all the points are aligned within the robot controller actual situation
                              # This allows to know at least if the points in the system are not synchronized with the RC
                              # For knowing the situation about every point, a more complex solution is needed

    # Status
    connected = None
    motor_on = None
    speed = 0

    def __init__(self, robot_id=None):
        self.robot_id = robot_id
        self.connected = False
        self.motor_on = False
        self.speed = 0
        self.points_synched = False
        self.points = {}

        self.executable_tasks = []  # List of all the executable tasks
        self.tasks_queue = []       # List of the previous/current/upcoming running tasks
        self.running_task = -1      # Index of the current running task in the queue
        self.continuous_executing = False  # Says whether or not the new enqueued task must be execute at the finishing of the previous one
        self.delay_between_tasks = 1 # 1 second of delay between one task and the next one (if continuous_executing is False)


    def __del__(self):
        self.disconnect()


    def _reset(self):
        self.connected = False
        self.motor_on = False
        self.points_synched = False

    def clone_without_tasks(self):
        copy = Robot(self.robot_id)
        copy.connected = self.connected
        copy.motor_on = self.motor_on
        copy.speed = self.speed
        copy.points_synched = self.points_synched
        copy.points = self.points
        copy.delay_between_tasks = self.delay_between_tasks
        return copy

    def connect(self):
        pass

    def disconnect(self):
        self._reset()


    def is_motor_on(self):
        """
        :return: True if motor is on, False if off, None if impossible to read
        """
        pass

    def turn_motor_on(self):
        pass

    def turn_motor_off(self):
        pass


    def take_arm(self):
        pass

    def give_arm(self):
        pass

    def move(self, point, pass_type="@0", interp="P"):
        """

        :param point:
        :param pass_type:
        :param interp: "P" is the fastest way to move (Point To Point, "L" (Linear) fallows a line, "A" (Arc), "S" (Spline)
        :return: True if there hasn't been a error during the movement, False otherwise
        """
        pass


    def move_once(self, point, interp="P"):
        """
        Take Arm -> Move -> Release Arm.
        Move the robot from its current position to the specified point.
        :param point:
        :return: True if there hasn't been a error during the movement, False otherwise
        """
        if self.connected:
            #pass_type = "@0"    # There are no other options because it's moving precisely in the specified point
            if self.take_arm():
                #moved = self.move(point, pass_type)
                moved = self.move(point, interp=interp)
                self.give_arm()
                return moved
        else:
            return False

    def halt(self):
        pass

    def get_current_position(self, data_type="P"):
        """
        :param data_type: "P" for retrieving the current position by P type data, "J" by J type data, "T" by T type data
        :return: 7-values array if data_type is "P", 8-values array if "J", 10-values array if "T", None if it's impossible to retrieve the current position
        """
        pass

    def set_current_position(self, new_position, data_type="P"):
        """

        :param new_position:
        :param data_type:
        :return: boolean
        """
        pass


    def move_xyz_offset(self, x=0, y=0, z=0, rx=0, ry=0, rz=0, coords=None):
        """

        :param x:
        :param y:
        :param z:
        :param rx:
        :param ry:
        :param rz:
        :param coords: Another (optional) way to express the coordinates offsets
        :return: True if there hasn't been a error during the movement, False otherwise
        """
        xyz_list = [x, y, z, rx, ry, rz]
        assert (coords is None or coords==[]) or (xyz_list==[0]*len(xyz_list) or coords==xyz_list), "Mismatch between parameters xyz coords and coords list."

        cur_pos = self.get_current_position(data_type="P")
        print(str(cur_pos))
        if cur_pos is None or cur_pos == []:
            return False

        coords_offsets = coords if coords!=None and coords!=[] else xyz_list
        coords_offsets += [0]*(len(self.DEFAULT_POINT_VALUES['P'])-len(coords_offsets))  # Fill the list with the remaining values as zeros
        new_pos = list(map(sum, zip(cur_pos, coords_offsets)))  # [cur_pos[0]+coords_offsets[0], cur_pos[1]+coords_offsets[1], ...]

        return self.set_current_position(new_position=new_pos, data_type="P")


    def move_joint_offset(self, j1=0, j2=0, j3=0, j4=0, j5=0, j6=0, j7=0, j8=0, joints=None):
        """
        Two ways for passing parameters, either by listing each joint offset or passing a list of joints.
        :param j1:
        :param j2:
        :param j3:
        :param j4:
        :param j5:
        :param j6:
        :param j8:
        :param joints: Another (optional) way to express the joints offsets
                       If None the singular params are evaluated, otherwise the params must be 0 or equals to the list
        :return: True if there hasn't been a error during the movement, False otherwise
        """
        j18_list = [j1, j2, j3, j4, j5, j6, j7, j8]
        assert (joints is None or joints==[]) or (j18_list==[0]*len(j18_list) or joints==j18_list), "Mismatch between parameters j1-8 and joints list."

        cur_pos = self.get_current_position(data_type="J")
        if cur_pos is None or cur_pos == []:
            return False

        joints_offsets = joints if joints!=None and joints!=[] else j18_list
        joints_offsets += [0]*(len(self.DEFAULT_POINT_VALUES['J'])-len(joints_offsets))  # Fill the list with the remaining values as zeros
        new_pos = list(map(sum, zip(cur_pos, joints_offsets)))  # [cur_pos[0]+joints_offset[0], cur_pos[1]+joints_offset[1], ...]

        return self.set_current_position(new_position=new_pos, data_type="J")


    def move_tool_offset(self, x=0, y=0, z=0, ox=0, oy=0, oz=0, ax=0, ay=0, az=0, coords=None):
        pass



    def set_speed(self, speed):
        """

        :param speed:
        :return: True if the speed has been changed, False otherwise
        """
        pass

    def get_speed(self):
        """

        :return: Value of the speed if it has been possible to read it, None otherwise
        """
        pass


    def init_points(self):
        self.points = {}
        for type in self.DEFAULT_POINT_VALUES:
            # From 0 to 99, add the type in front of th number, generate a dict with that key and the corresponding intial values, and append to the final dict
            self.points.update({type+str(i): self.DEFAULT_POINT_VALUES[type] for i in range(self.POINTS_PER_TYPE)})


    def load_point(self, point_name):
        """

        :param point_name:
        :return: True if the specified point has been loaded into the system, False if not
        """
        pass

    def load_points(self):
        """

        :return: True if all the points have been loaded into the system, False if not
        """
        committed = True
        for point_name in self.points:
            committed &= self.load_point(point_name)  # If just one of these loads goes wrong, then committed becomes False
        return committed

    def dump_point(self, point_name):
        """

        :param point_name:
        :return: True if the specified point (in the system) has been stored into the robot controller, False if not
        """
        pass

    def dump_points(self):
        """
        This operation also update the status of points_synched to True if everything goes fine.
        :return: True if all the system points have been stored into the robot controller, False if not
        """
        committed = True
        for point_name in self.points:
            committed &= self.dump_point(point_name)  # If just one of these loads goes wrong, then committed becomes False
        self.points_synched = committed
        return committed

    def set_point(self, point_name, new_value, synch=False):
        """

        :param point_name:
        :param new_value:
        :param synch: if True the new_value is immediately dumped on the robot controller
        :return: True if the point has been set and, in case (if synch is True), also dumped on the robot controller
        """
        type = point_name[0].upper()
        assert len(new_value) == len(self.DEFAULT_POINT_VALUES[type]), "The new point value is invalid (length mismatch)."
        self.points[point_name] = new_value
        if synch:
            point_dumped = self.dump_point(point_name)
            self.points_synched &= point_dumped
            return point_dumped
        else:
            self.points_synched = False
        return True

    def get_point(self, point_name, synch=False):
        """

        :param point_name:
        :param new_value:
        :param synch: if True the new_value is immediately dumped on the robot controller
        :return: The point (after having loaded it from the RC if synch id True), None if the loading failed
        """
        if synch:
            point_loaded = self.load_point(point_name)
            return self.points[point_name] if point_loaded else None
        else:
            return self.points[point_name]

    def delete_point(self, point_name, synch=False):
        type = point_name[0].upper()
        return self.set_point(point_name, self.DEFAULT_POINT_VALUES[type], synch)


    def add_executable_task(self, new_task):
        self.executable_tasks.append(new_task)

    def get_executable_task(self, task_name):
        for task in self.executable_tasks:
            if task.task_name == str(task_name):
                return task
        return None

    def delete_executable_task(self, task_name):
        """
        :param task_name:
        :return: True if the task has been found and deleted, False otherwise
        """
        task = self.get_executable_task(task_name)
        if task != None:
            self.executable_tasks.remove(task)
            del task
            return True
        else:
            return False

    def get_executable_tasks(self):
        return self.executable_tasks

    def enqueue_task(self, task_name):
        task = self.get_executable_task(task_name)
        if task is not None:
            self.tasks_queue.append(task.clone())
            return True
        else:
            return False


    def get_tasks_running_quque(self):
        return self.tasks_queue

    def clear_tasks_running_quque(self):
        if not self.is_running_task():
            for t in self.tasks_queue:
                del t
            self.tasks_queue = []
            self.running_task = -1
            return True
        else:
            return False

    def is_running_task(self):
        current_task = self.get_running_task()
        if current_task != None:
            return True if current_task.status == Task.STATUS_STATE_EXECUTING else False
        return False


    def get_running_task(self):
        i = self.running_task
        if i != -1:
            if self.tasks_queue[i].status != Task.STATUS_STATE_DONE and self.tasks_queue[i].status != Task.STATUS_STATE_HALTED:
                return self.tasks_queue[i]
        return None

    def _set_next_running_task(self):
        if self.running_task < len(self.tasks_queue)-1:
            self.running_task += 1
            return True
        else:
            return False

    def execute_next_task(self):
        """

        :return: True if a new task has been executed, False if it hasn't been possible
                 (there are no tasks in queue or the prev task is still executing)
        """
        i = self.running_task
        if i != -1:
            if self.tasks_queue[i].status == Task.STATUS_STATE_DONE or self.tasks_queue[i].status == Task.STATUS_STATE_HALTED:
                proceed = self._set_next_running_task()  # Return False if there is no new task enqueued
            else:
                return False  # The task hasn't finished yet
        else:
            proceed = self._set_next_running_task()  # Return False if there is no new task enqueued
        if proceed:
            logging.info("Executing Task: " + str(self.tasks_queue[self.running_task].serialize()))
            self.tasks_queue[self.running_task].start()
            return True
        else:
            return False

    def execute_all_queued_tasks(self, delay_between_tasks):
        self.continuous_executing = True  # Says whether or not the new enqueued task must be execute at the finishing of the previous one
        self.delay_between_tasks = delay_between_tasks
        return self.execute_next_task()

    def halt_running_queue(self):
        self.continuous_executing = False  # Stop the execution of next tasks
        running_task = self.get_running_task()
        if running_task != None:
            running_task.force_stop()

    def notify_task_status(self, status):
        """
        This method can be called by a running task through its reference to the parent robot (observer).
        The method will either wait or execute a next task in the queue according to the value of continuous_executing;
        NB: if continuous_executing has been set to True, it'll be set to False as soon as the last task in the queue has been executed
        :return:
        """
        if status == Task.STATUS_STATE_DONE:
            if self.continuous_executing:
                time.sleep(self.delay_between_tasks)
                task_executed = self.execute_next_task()
                if not task_executed:
                    self.continuous_executing = False  # Reset it to False
            else:
                pass
        elif status == Task.STATUS_STATE_HALTED:
            pass


    def serialize(self):
        return {
            'robot_id': self.robot_id,
            'connected': self.connected,
            'motor_on': self.motor_on,
            'speed': self.speed,
            'executable_tasks': [t.serialize() for t in self.executable_tasks],
            'tasks_queue': [t.serialize() for t in self.tasks_queue],
            'running_task': self.get_running_task().serialize() if self.get_running_task() != None else None,
        }

#End class
