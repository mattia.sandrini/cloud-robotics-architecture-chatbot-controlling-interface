# author: Emilio Baresi

import functions
import json


context= ""


while True:

    msg = input("--------------> ")


    answer = functions.sendMessageToConversation(msg, context)
    answerJSON = answer.json()
    context = answerJSON['context']
    node_visited = answerJSON['output']['nodes_visited']

    if len(answerJSON['intents'])>0:
        intent = answerJSON['intents'][0]['intent']
    else:
        intent = ""
    if len(answerJSON['entities'])>0:
        entity = answerJSON['entities'][0]
    else:
        entity = ""
    output = ""
    for i in range(0,len(answerJSON['output']['text'])):
        output+= answerJSON['output']['text'][i] + "\n"


    context, ret = functions.handleIntent(intent, entity, output, context)


    print(ret)

    context = json.dumps(context)
