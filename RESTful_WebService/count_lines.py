# author: Mattia Sandrini 

from pathlib import Path
import pygount  # Must be installed from package manager or pip

def recursively_count(path, extensions=['.py'], skip_names=[]):
    code, documentation = 0, 0
    for x in path.iterdir():
        if x.is_dir():
            if x.name not in skip_names:
                #print("Directory: " + str(x))
                r_code, r_documentation = recursively_count(x, extensions, skip_names)
                code += r_code
                documentation += r_documentation
        else:
            if x.suffix in extensions and x.name not in skip_names:
                #print("File:" + str(x))
                #print(x.suffix)
                analysis = pygount.source_analysis(x.absolute(), 'pygount')
                code += analysis.code
                documentation += analysis.documentation
    return code, documentation

if __name__ == '__main__':
    #analysis = pygount.source_analysis('C:\\Users\\Prog\\Desktop\\robotica\\main.py', 'pygount')
    path = Path('.')
    code, documentation = recursively_count(path, extensions=['.py'], skip_names=['pybcapclient'])
    print("  PYTHON  -> Code: %5d, Documentation: %5d" % (code, documentation))

    path = Path('.')
    code, documentation = recursively_count(path, extensions=['.html', 'htm', '.js', '.css'], skip_names=['pybcapclient', 'count_lines'])
    print("HTML + JS -> Code: %5d, Documentation: %5d" % (code, documentation))

    path = Path('.')
    code, documentation = recursively_count(path, extensions=['.py', '.html', 'htm', '.js', '.css'], skip_names=['pybcapclient'])
    print("  WHOLE   -> Code: %5d, Documentation: %5d" % (code, documentation))
