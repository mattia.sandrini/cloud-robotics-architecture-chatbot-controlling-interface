# author: Emilio Baresi

import json
import watson_developer_cloud

#Token for the IBM services
conversation = watson_developer_cloud.ConversationV1(
    username='b1f28c8b-4d8e-4129-a40c-bef8f2f4b908',
    password='4qaRgSAqBjoL',
    version='2018-02-16'
)

w_id = '0590704c-9285-4cd0-bb5a-330aa2e02e1f'


#function that load the robot list in the watson workspace
def createRobotList(robots):
    try:
        conversation.delete_entity(
            workspace_id = w_id,
            entity='robots'
        )
    except Exception:
        print("Entity already deleted")

    val = []
    for i in range(0, len(robots)):
        val.insert(0,{'value': robots[i]['robot_id']})

    response = conversation.create_entity(
        workspace_id = w_id,
        entity = 'robots',
        values = val
    )

#function that load the task list in the watson workspace
def createTasksList(tasks):
    try:
        conversation.delete_entity(
            workspace_id = w_id,
            entity='tasks'
        )
    except Exception:
        print("Entity already deleted")

    val = []
    for i in range(0, len(tasks)):
        val.insert(0,{'value': tasks[i]['task_name']})

    response = conversation.create_entity(
        workspace_id = w_id,
        entity = 'tasks',
        values = val
    )
