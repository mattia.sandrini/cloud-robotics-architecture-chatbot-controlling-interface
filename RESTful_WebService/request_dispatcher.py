# author: Mattia Sandrini 

from threading import Thread

from flask_socketio import SocketIO
from flask import Flask, request, render_template, json
from werkzeug import exceptions
import re

from RAN.robots.denso import Denso
from RAN.robots.dummy import Dummy
from RAN.robotic_cell import RoboticCell
from RAN.tasks.task import Task
from RAN.tasks.do_nothing import DoNothing
from RAN.tasks.pick_and_place import PickAndPlace
from RAN.tasks.task_steps.take_arm import TakeArm
from RAN.tasks.task_steps.give_arm import GiveArm
from RAN.tasks.task_steps.wait import Wait
from RAN.tasks.task_steps.set_speed import SetSpeed
from RAN.tasks.task_steps.move_to import MoveTo

__author__ = 'Mattia Sandrini'

app = Flask(__name__)
socketio = SocketIO(app)  # Turn the flask app into a socketio app


FILE_XML = 'file.xml'
cell = None          # Initialized through XML Deserialization when GET "/robots" is requested

ROBOT_NOT_FOUND_MSG = "Robot \"{}\" not found."
POINT_NOT_FOUND_MSG = "Point \"{}\" not found."
TASK_NOT_FOUND_MSG = "Task \"{}\" not found."


# Utility function for converting a string/int into boolean
def convert2bool(v):
    if type(v) == bool:
        return v
    elif type(v) == int:
        return v != 0    # Return False if v == 0, otherwise True
    elif type(v) == str:
        return v.lower() in ("true", "t", "1")
    else:
        return None


def deserialize_system():
    global cell
    cell = RoboticCell()  # Will contain the whole robotic_cell structure
    return cell.deserialize_xml(FILE_XML)

@app.route('/serialize', methods=['GET', 'POST', 'PUT'])
def serialize_robotic_cell():
    global cell
    if cell.serialize_xml(FILE_XML):
        return json.dumps({'committed': True}), 200
    else:
        return json.dumps({'committed': False, 'error_msg': 'Impossible to serialize the system.'}), 500

@app.route('/deserialize', methods=['GET', 'POST', 'PUT'])
def deserialize_robotic_cell():
    deserialization_success = deserialize_system()
    if deserialization_success:
        return json.dumps({'committed': True}), 200
    else:
        return json.dumps({'committed': False, 'error_msg': 'Impossible to serialize the system.'}), 500


@app.route('/', methods=['GET'])
def index():
    global cell
    return render_template('index.html', host=request.host, robotic_cell=cell.serialize())


@app.route('/robots', methods=['GET'])
def get_robots_list():
    global cell
    return json.dumps(cell.serialize()), 200


@app.route('/robots/<robot_id>', methods=['GET'])
def get_robot(robot_id):
    robot = cell.search_robot(robot_id)
    if robot != None:
        print("Found robot \"{}\"!".format(robot.robot_id))
        return json.dumps(robot.serialize()), 200
    else:
        return ROBOT_NOT_FOUND_MSG.format(robot_id), 404 # Not Found

def is_valid_host(host):
    result = True
    aa = re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$", host)
    if aa:
        for n in list(map(int, host.split('.'))):
            result &= n in range(0, 256)
    return result

def is_valid_port(port):
    return port.isdigit() and (int(port) in range(1024, 65535))

@app.route('/robots/<robot_id>', methods=['PUT'])
def add_robot(robot_id):
    robot = cell.search_robot(robot_id)
    if robot == None:
        try:
            host = request.values['host']
            port = request.values['port']
            if host == None or not is_valid_host(host) or \
               port == None or not is_valid_port(port):
                new_robot = Dummy(robot_id)
            else:
                new_robot = Denso(robot_id, host, int(port))
            new_robot.init_points()
            cell.robots.append(new_robot)
            return json.dumps({'robot': new_robot.serialize(), 'committed': True}), 201
        except exceptions.BadRequestKeyError as e:
            print("Exception:" + str(e))
            return json.dumps({'committed': False, 'error_msg': "Syntax error in key names.\n"+str(e)}), 400  # Bad Request (syntax error in key names)
    else:
        return json.dumps({'committed': False, 'error_msg': "Robot \"{}\" already in the system.".format(robot_id)}), 400 # Bad Request (the robot is already registered)

@app.route('/robots/<robot_id>', methods=['POST'])
def modify_robot(robot_id):
    robot = cell.search_robot(robot_id)
    if robot != None:
        print("Found and modified robot \"{}\"!".format(robot.robot_id))
        try:
            robot.robot_id = request.values['robot_id']
            robot.host = request.values['host']
            robot.port = int(request.values['port'])
            return json.dumps({'robot': robot.serialize(), 'committed': True}), 200
        except exceptions.BadRequestKeyError as e:
            return json.dumps({'committed': False, 'error_msg': "Syntax error in key names.\n"+str(e)}), 400  # Bad Request (syntax error in key names)
        except Exception as e:
            return json.dumps({'committed': False, 'error_msg': "Error in modifying the robot.\n"+str(e)}), 400  # Bad Request (syntax error in key values)
    else:
        return json.dumps({'committed': False, 'error_msg': ROBOT_NOT_FOUND_MSG.format(robot_id)}), 404 # Not Found

@app.route('/robots/<robot_id>', methods=['DELETE'])
def delete_robot(robot_id):
    print("DELETE robot " + robot_id)
    robot = cell.search_robot(robot_id)
    if robot != None:
        print("Found and deleted the robot \"{}\"!".format(robot.robot_id))
        cell.delete_robot(robot_id)
        return json.dumps({'committed': True}), 205  # Reset Content (request successfully processed, but returning no content. Requires that the requester reset the document view)
    else:
        return json.dumps({'committed': False, 'error_msg': ROBOT_NOT_FOUND_MSG.format(robot_id)}), 404 # Not Found


##############################################
################### STATUS ###################
##############################################

@app.route('/robots/<robot_id>/status', methods=['GET'])
def get_robot_status(robot_id):
    robot = cell.search_robot(robot_id)
    if robot != None:
        try:
            field = request.values['field']
            if field == 'connected':
                return json.dumps({'connected': robot.connected, 'committed': True}), 200
            elif field == 'motor_on':
                motor_on = robot.is_motor_on()
                if motor_on != None:
                    return json.dumps({'motor_on': motor_on, 'committed': True}), 200
                else:
                    return json.dumps({'committed': False, 'error_msg': "Motor status retrieving failed."}), 202 #(202) Accepted request, but impossible to carry out
            elif field == 'speed':
                speed = robot.get_speed()
                if speed != None:
                    return json.dumps({'speed': speed, 'committed': True}), 200
                else:
                    return json.dumps({'committed': False, 'error_msg': "Speed retrieving failed."}), 202 #(202) Accepted request, but impossible to carry out
            elif field == 'current_position':
                data_type = request.values['data_type']
                cur_pos = robot.get_current_position(data_type=data_type)
                #cur_pos = robot.DEFAULT_POINT_VALUES[data_type]
                if cur_pos != None:
                    return json.dumps({'committed': True, 'current_position': {'value': cur_pos, 'data_type': data_type}}), 200
                else:
                    return json.dumps({'committed': False, 'error_msg': "Current position retrieving failed."}), 202 #(202) Accepted request, but impossible to carry out
            else:
                return json.dumps({'committed': False, 'error_msg': "Syntax error in \"field\" value."}), 400  # Bad Request (syntax error in field value)
        except exceptions.BadRequestKeyError as e:
            return json.dumps({'committed': False, 'error_msg': "Syntax error in key names.\n"+str(e)}), 400  # Bad Request (syntax error in key names)
    else:
        return json.dumps({'committed': False, 'error_msg': ROBOT_NOT_FOUND_MSG.format(robot_id)}), 404 # Not Found

@app.route('/robots/<robot_id>/status', methods=['POST'])
def set_robot_status(robot_id):
    robot = cell.search_robot(robot_id)
    if robot != None:
        try:
            field = request.values['field']
            if field == 'connected':
                # Convert the string 'true'/'false'/'True'/'False' in boolean True/False
                value = convert2bool(request.values['value'])
                print("Post status: " + field + " - Value: " + str(value))
                if value == True:
                    committed = robot.connect()
                    if committed:
                        return json.dumps({'committed': True, 'connected': robot.connected}), 200
                    else:
                        return json.dumps({'committed': False, 'connected': robot.connected, 'error_msg': "Connection failed."}), 202 # (202) Accepted request, but impossible to carry out
                else:
                    robot.disconnect()
                    return json.dumps({'connected': False, 'committed': True}), 200
            elif field == 'motor_on':
                if robot.connected:
                    # Convert the string 'true'/'false'/'True'/'False' in boolean True/False
                    value = convert2bool(request.values['value'])
                    if value == True:
                        committed = robot.turn_motor_on()
                    else:
                        committed = robot.turn_motor_off()
                    if committed:
                        return json.dumps({'committed': True, 'motor_on': robot.is_motor_on()}), 200
                    else:
                        return json.dumps({'committed': False, 'motor_on': robot.is_motor_on(), 'error_msg': "Motor setting failed."}), 202 # (202) Accepted request, but impossible to carry out
                else:
                    return json.dumps({'committed': False, 'error_msg': "Connection with the robot is needed."}), 400  # Bad Request

            elif field == 'speed':
                if robot.connected:
                    # Convert the string 'true'/'false'/'True'/'False' in boolean True/False
                    try:
                        value = int(request.values['value'])
                    except ValueError as e:
                        return json.dumps({'committed': False, 'error_msg': "Speed value must be an integer."}), 400  # Bad Request
                    committed = robot.set_speed(value)
                    if committed:
                        return json.dumps({'committed': True, 'speed': value}), 200
                    else:
                        return json.dumps({'committed': False, 'speed': value, 'error_msg': "Speed setting failed."}), 202 # (202) Accepted request, but impossible to carry out
                else:
                    return json.dumps({'committed': False, 'error_msg': "Connection with the robot is needed."}), 400  # Bad Request
            elif field == 'current_position':
                try:
                    new_value = json.loads(request.values['value'])  # The list in data_post request must be stringified
                    data_type = request.values['type']
                    if robot.set_current_position(new_value, data_type):
                        cur_pos = robot.get_current_position(data_type=data_type)
                        #cur_pos = robot.DEFAULT_POINT_VALUES[data_type]
                        if cur_pos != None:
                            return json.dumps({'committed': True, 'current_position': {'value': cur_pos, 'data_type': data_type}}), 200
                        else:
                            return json.dumps({'committed': False, 'error_msg': "Current position retrieving failed."}), 202 #(202) Accepted request, but impossible to carry out
                    else:
                        return json.dumps({'committed': False, 'error_msg': "Movement in new position failed."}), 202 #(202) Accepted request, but impossible to carry out
                except exceptions.BadRequestKeyError as e:
                    return json.dumps({'committed': False, 'error_msg': "Syntax error in key names.\n"+str(e)}), 400  # Bad Request (syntax error in key names)
            else:
                return json.dumps({'committed': False, 'error_msg': "Syntax error in \"field\" value."}), 400  # Bad Request (syntax error in field value)
        except exceptions.BadRequestKeyError as e:
            return json.dumps({'committed': False, 'error_msg': "Syntax error in key names.\n"+str(e)}), 400  # Bad Request (syntax error in key names)
    else:
        return json.dumps({'committed': False, 'error_msg': ROBOT_NOT_FOUND_MSG.format(robot_id)}), 404 # Not Found


###############################################
#################### TASKS ####################
###############################################

@socketio.on('connect', namespace='/monitor')
def monitor_connect():
    print('Client connected')

@socketio.on('disconnect', namespace='/monitor')
def monitor_disconnect():
    print('Client disconnected')


@app.route('/robots/<robot_id>/tasks/running_queue', methods=['POST'])
def task_queue_action(robot_id):
    robot = cell.search_robot(robot_id)
    if robot != None:
        try:
            if request.values['action'].lower() == 'enqueue_task':
                task_name = request.values['task_name']
                is_task_enqueued = robot.enqueue_task(task_name)
                if is_task_enqueued:
                    return json.dumps({'committed': True, 'tasks_running_queue': [t.serialize() for t in robot.get_tasks_running_quque()]}), 200
                else:
                    return json.dumps({'committed': False, 'error_msg': "Impossible to enqueue the task."}), 202
            if request.values['action'].lower() == 'execute_all':
                try:
                    delay = int(request.values['delay'])
                except:
                    delay = 0
                is_task_executed = robot.execute_all_queued_tasks(delay_between_tasks=delay)
                if is_task_executed:
                    task = robot.get_running_task()
                    # ...
                else:
                    return json.dumps({'committed': False, 'error_msg': "Impossible to execute the task."}), 202
                return json.dumps({'committed': True, 'tasks_running_queue': [t.serialize() for t in robot.get_tasks_running_quque()]}), 202 # Accepted
            elif request.values['action'].lower() == 'execute_next':
                is_task_executed = robot.execute_next_task()
                if is_task_executed:
                    task = robot.get_running_task()
                    # ...
                else:
                    return json.dumps({'committed': False, 'error_msg': "Impossible to execute the task."}), 202
                return json.dumps({'committed': True, 'tasks_running_queue': [t.serialize() for t in robot.get_tasks_running_quque()]}), 202 # Accepted
            elif request.values['action'].lower() == 'halt':
                try:
                    robot.halt_running_queue()
                    return json.dumps({'committed': True, 'tasks_running_queue': [t.serialize() for t in robot.get_tasks_running_quque()]}), 202 # Accepted
                except Exception as e:
                    print("Exception: " + str(e))
                    return json.dumps({'committed': False, 'error_msg': "Impossible to halt the running queue."}), 202
            else:
                pass
        except exceptions.BadRequestKeyError as e:
            return json.dumps({'committed': False, 'error_msg': "Syntax error in key names.\n"+str(e)}), 400  # Bad Request (syntax error in key names)
    else:
        return json.dumps({'committed': False, 'error_msg': ROBOT_NOT_FOUND_MSG.format(robot_id)}), 404 # Not Found

@app.route('/robots/<robot_id>/tasks/running_queue', methods=['GET','DELETE'])
def tasks_queue(robot_id):
    robot = cell.search_robot(robot_id)
    if robot != None:
        if request.method == 'GET':
            return json.dumps({'committed': True, 'tasks_running_queue': [t.serialize() for t in robot.get_tasks_running_quque()]}), 200
        elif request.method == 'DELETE':
            committed = robot.clear_tasks_running_quque()
            if committed:
                return json.dumps({'committed': True}), 200
            else:
                return json.dumps({'committed': False, 'error_msg': "Impossible to clear tasks running queue."}), 202
    else:
        return json.dumps({'committed': False, 'error_msg': ROBOT_NOT_FOUND_MSG.format(robot_id)}), 404 # Not Found



def generate_executable_task(type, task_name, robot, repetitions=1, repetitions_delay=0, params=None):
    #assert type in Task.TASK_TYPES, ...
    task = Task(task_name, robot, type=type, socketio=socketio)
    if type == Task.TASK_TYPE_CUSTOM:
        for i in range(repetitions):
            for step in params['steps']:
                if step['step_type'] == 'take_arm':
                    task.add_step(TakeArm(parent_task=task))
                elif step['step_type'] == 'give_arm':
                    task.add_step(GiveArm(parent_task=task))
                elif step['step_type'] == 'wait':
                    task.add_step(Wait(parent_task=task, time_ms=step['value']))
                elif step['step_type'] == 'set_speed':
                    task.add_step(SetSpeed(parent_task=task, speed=step['value']))
                elif step['step_type'] == 'move_to':
                    task.add_step(MoveTo(parent_task=task, point=step['point_name'], type=step['pass_type'], interp=step['interp']))
            if i < repetitions-1:
                task.add_step(Wait(parent_task=task, time_ms=repetitions_delay))
    elif type == Task.TASK_TYPE_DO_NOTHING:
        task = DoNothing(task_name, robot, socketio)
    elif type == Task.TASK_TYPE_PICK_AND_PLACE:
        if params is None or params == []:
            task = PickAndPlace(task_name, robot, socketio, repeat_times=repetitions, delay_between_repetitions=repetitions_delay)
        else:
            task = PickAndPlace(task_name, robot, socketio, points=params['points'], repeat_times=repetitions, delay_between_repetitions=repetitions_delay)
    return task

@app.route('/robots/<robot_id>/tasks/executables', methods=['GET'])
def get_robot_executable_tasks(robot_id):
    robot = cell.search_robot(robot_id)
    if robot != None:
        return json.dumps({'committed': True, 'executable_tasks': [t.serialize() for t in robot.get_executable_tasks()]}), 200
    else:
        return json.dumps({'committed': False, 'error_msg': ROBOT_NOT_FOUND_MSG.format(robot_id)}), 404 # Not Found

@app.route('/robots/<robot_id>/tasks/executables/<task_name>', methods=['GET', 'POST', 'PUT', 'DELETE'])
def robot_executable_task(robot_id, task_name):
    robot = cell.search_robot(robot_id)
    if robot != None:
        try:
            if task_name in [t.task_name for t in robot.get_executable_tasks()]:
                if request.method == 'GET':
                    task = robot.get_executable_task(task_name)
                    return json.dumps({'committed': True, 'executable_task': task.serialize()}), 200
                elif request.method == 'POST':
                    #new_value = json.loads(request.values['value'])
                    #request.values['value']
                    #task = robot.get_executable_task(task_name)
                    #return json.dumps({'committed': True, 'executable_task': task.serialize()}), 200
                    return json.dumps({'committed': False, 'error_msg': "This functionality hasn't been implemented yet"}), 501 # Not Implemented
                elif request.method == 'DELETE':
                    committed = robot.delete_executable_task(task_name)
                    if committed:
                        return json.dumps({'committed': True}), 200
                    else:
                        return json.dumps({'committed': False, 'error_msg': "Impossible to delete executable task."}), 202
                elif request.method == 'PUT':
                    return json.dumps({'committed': False, 'error_msg': "Task \"{}\" already in the system.".format(task_name)}), 400 # Bad Request (the task is already registered)
            else:
                if request.method == 'PUT':
                    task_type = request.values['type']
                    repetitions = int(request.values['task_repetitions'])
                    repetitions_delay = int(request.values['repetitions_delay'])
                    params = json.loads(request.values['params'])
                    new_task = generate_executable_task(task_type, task_name, robot, repetitions, repetitions_delay, params)
                    robot.add_executable_task(new_task)
                    return json.dumps({'committed': True, 'executable_task': new_task.serialize()}), 200
                else:
                    return json.dumps({'committed': False, 'error_msg': TASK_NOT_FOUND_MSG.format(task_name)}), 404 # Not Found
        except exceptions.BadRequestKeyError as e:
            return json.dumps({'committed': False, 'error_msg': "Syntax error in key names.\n"+str(e)}), 400  # Bad Request (syntax error in key names)
    else:
        return json.dumps({'committed': False, 'error_msg': ROBOT_NOT_FOUND_MSG.format(robot_id)}), 404 # Not Found



@app.route('/robots/<robot_id>/points/<point_name>', methods=['GET', 'POST', 'DELETE'])
def robot_point(robot_id, point_name):
    robot = cell.search_robot(robot_id)
    if robot != None:
        try:
            synch = convert2bool(request.values['synch'])
        except exceptions.BadRequestKeyError as e:
            return json.dumps({'committed': False, 'error_msg': "Syntax error in key names.\n"+str(e)}), 400  # Bad Request (syntax error in key names)

        point_name = point_name.upper()
        if point_name in robot.points:
            if request.method == 'GET':
                point_value = robot.get_point(point_name, synch)
                if point_value != None:
                    return json.dumps({'committed': True, 'point': {'point_name': point_name, 'value': point_value}}), 200
                else:
                    return json.dumps({'committed': False, 'error_msg': "Point retrieving failed."}), 202 #(202) Accepted request, but impossible to carry out
            elif request.method == 'POST':
                try:
                    new_value = json.loads(request.values['value'])  # The list in data_post request must be stringified
                    if robot.set_point(point_name, new_value, synch):
                        return json.dumps({'committed': True}), 200
                    else:
                        return json.dumps({'committed': False, 'error_msg': "Point modification failed."}), 202 #(202) Accepted request, but impossible to carry out
                except exceptions.BadRequestKeyError as e:
                    return json.dumps({'committed': False, 'error_msg': "Syntax error in key names.\n"+str(e)}), 400  # Bad Request (syntax error in key names)
            elif request.method == 'DELETE':
                if robot.delete_point(point_name, synch):
                    return json.dumps({'committed': True}), 200
                else:
                    return json.dumps({'committed': False, 'error_msg': "Point deletion failed."}), 202 #(202) Accepted request, but impossible to carry out
        else:
            return json.dumps({'committed': False, 'error_msg': POINT_NOT_FOUND_MSG.format(point_name)}), 404 # Not Found
    else:
        return json.dumps({'committed': False, 'error_msg': ROBOT_NOT_FOUND_MSG.format(robot_id)}), 404 # Not Found



@app.route('/robots/<robot_id>/commands/move', methods=['GET', 'POST']) # GET and POST provide the same result
def robot_move_action(robot_id):
    robot = cell.search_robot(robot_id)
    if robot != None:
        type = request.values['type']
        if type != None and (type in ['tool_offset', 'joint_offset', 'xyz_offset', 'xyz_offset', 'absolute_point']):
            if type == "tool_offset":
                # ...
                return json.dumps({'committed': False, 'error_msg': "This functionality hasn't been implemented yet"}), 501 # Not Implemented
            elif type == "joint_offset":
                try:
                    joint = request.values['label'].upper()
                    sign = request.values['sign']
                    step = request.values['step']
                    if joint in ['J1','J2','J3','J4','J5','J6','J7','J8'] and sign in ['+','-'] and step.isdigit():
                        step = int(step)
                        value = step if sign=='+' else -step

                        values = [value*int(joint==c) for c in ['J1','J2','J3','J4','J5','J6','J7','J8']]  # values = [0,value,0,...,0]  if joint == 'J2'
                        committed = robot.move_joint_offset(joints=values)
                        if committed:
                            cur_pos = robot.get_current_position(data_type="J")
                            return json.dumps({'committed': True, 'current_position': {'value': cur_pos, 'data_type': "J"}}), 200
                        else:
                            return json.dumps({'committed': False, 'error_msg': "Motion failed."}), 202 #(202) Accepted request, but impossible to carry out
                    else:
                        return json.dumps({'committed': False, 'error_msg': "Values error in key-values data."}), 400  # Bad Request (Value Error)
                except exceptions.BadRequestKeyError as e:
                    return json.dumps({'committed': False, 'error_msg': "Syntax error in key names.\n"+str(e)}), 400  # Bad Request (syntax error in key names)
            elif type == "xyz_offset":
                try:
                    coord = request.values['label'].upper()
                    sign = request.values['sign']
                    step = request.values['step']

                    if coord in ['X','Y','Z','RX','RY','RZ'] and sign in ['+','-'] and step.isdigit():
                        step = int(step)
                        value = step if sign=='+' else -step

                        values = [value*int(coord==c) for c in ['X','Y','Z','RX','RY','RZ']]  # values = [0, value, 0]  if coord == 'Y'
                        committed = robot.move_xyz_offset(coords=values)

                        if committed:
                            cur_pos = robot.get_current_position(data_type="P")
                            return json.dumps({'committed': True, 'current_position': {'value': cur_pos, 'data_type': "P"}}), 200
                        else:
                            return json.dumps({'committed': False, 'error_msg': "Motion failed."}), 202 #(202) Accepted request, but impossible to carry out
                    else:
                        return json.dumps({'committed': False, 'error_msg': "Values error in key-values data."}), 400  # Bad Request (Value Error)
                except exceptions.BadRequestKeyError as e:
                    return json.dumps({'committed': False, 'error_msg': "Syntax error in key names.\n"+str(e)}), 400  # Bad Request (syntax error in key names)
            elif type == "absolute_point":
                try:
                    point_name = request.values['point_name'].upper()
                    data_type = point_name[0]
                    if robot.move_once(point=point_name):
                        cur_pos = robot.get_current_position(data_type=data_type)
                        return json.dumps({'committed': True, 'current_position': {'value': cur_pos, 'data_type': data_type}}), 200
                    else:
                        return json.dumps({'committed': False, 'error_msg': "Motion failed."}), 202 #(202) Accepted request, but impossible to carry out
                except exceptions.BadRequestKeyError as e:
                    return json.dumps({'committed': False, 'error_msg': "Syntax error in key names.\n"+str(e)}), 400  # Bad Request (syntax error in key names)
            else:
                return json.dumps({'committed': False, 'error_msg': "Values error in key-values data."}), 400  # Bad Request (Value Error)
        else:
            return json.dumps({'committed': False, 'error_msg': "Values error in key-values data."}), 400  # Bad Request (Value Error)
    else:
        return json.dumps({'committed': False, 'error_msg': ROBOT_NOT_FOUND_MSG.format(robot_id)}), 404 # Not Found





###############################################
################### UTILITY ###################
###############################################

def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()

@app.route('/shutdown', methods=['POST', 'GET', 'PUT'])
def shutdown():
    # Release all the resources (robots, ...)
    # ...
    shutdown_server()
    return 'Server shutting down...'

@app.after_request
def add_header(r):
    """
    To avoid caching of js files when html pages are requested.
    """
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate, public, max-age=0"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    return r


