import time
from .task_step import TaskStep

__author__ = 'Mattia Sandrini'

class Wait(TaskStep):

    def __init__(self, name="wait", parent_task=None, time_ms=0):
        self.value = time_ms
        super(Wait, self).__init__(name, parent_task)

    def execute(self):
        if not self._thread_stop_event.isSet():
            time.sleep(self.value/1000)

    def clone(self):
        return Wait(name=self.name, parent_task=self.parent_task, time_ms=self.value)

    def serialize(self):
        return {
            'name': self.name,
            'value': self.value
        }
