
var host = document.domain + ':' + location.port;
var robot_id = null;

var pnt_field_labels = new Array();
pnt_field_labels['P'] = ['X', 'Y', 'Z', 'RX', 'RY', 'RZ', 'Fig'];
pnt_field_labels['J'] = ['J1', 'J2', 'J3', 'J4', 'J5', 'J6', 'J7', 'J8'];
pnt_field_labels['T'] = ['X', 'Y', 'Z', 'Ox', 'Oy', 'Oz', 'Ax', 'Ay', 'Az', 'Fig'];


function write_point(synch) {   // synch=false -> SAVE;  synch=true -> DUMP
        var robot_id = window.robot_id;
        var point_name = $("#point_name").val();
        var point_value = get_point_text_fields_values();  // Array of float
        point_value = JSON.stringify(point_value);  // Stringify the array
        $.post("http://{0}/robots/{1}/points/{2}".format(host, robot_id, point_name), { 'synch': synch, 'value': point_value }, function(result, status) {
            if (status == "success") {
                result = JSON.parse(result);
                if (result['committed'] == true)  // The point has been saved on the system
                {
                    msg = "Point \"{0}\" {1}.".format(point_name, synch ? 'dumped on the Robot Controller' : 'saved on the System');
                    show_success_alert(msg);
                }
            }
            if (result['committed'] == false) {
                //alert("Error in writing the point on the system!\nError msg: " + result['error_msg']);
                show_error_alert(result['error_msg'].replace('\n', '<br />'));
            }
        });
    }

function read_point(point_name, synch) {
    var uri = "http://{0}/robots/{1}/points/{2}".format(host, robot_id, point_name);
    $.get(uri, { 'synch': synch }, function(data, status) {
        result = JSON.parse(data);
        if (status == "success") {
            if (result['committed']) {
                point_value = result['point']['value'];   // point_value = Array()
                for (var i=0;i<point_value.length; i++)
                    $("#point_value_{0}".format(i)).val(point_value[i]);

                if (synch)
                    show_success_alert("Point \"{0}\" loaded from the Robot Controller.".format(point_name));  // Show alert only when the load comes from the robot
            }
        }
        if (result['committed'] == false) {
            //alert("Error in loading the point to the system!\nError msg: " + result['error_msg']);
            show_error_alert(result['error_msg'].replace('\n', '<br />'));
        }
    });
}


function update_select_point_names(point_type)
{
    var options = "";
    for (var i=0; i<100; i++)
        options += "<option value='{0}{1}'>{0}{1}</option>".format(point_type, i);

    $("#point_name").html(options);
    $("#destination_point_name").html(options);
}

function generate_point_text_fields(point_type)
{
    var text = "";
    for (var i=0; i<pnt_field_labels[point_type].length; i++)
    {
        text += "<div class='form-group row'>" +
                "    <label for='point_value_{0}' class='col-lg-2 col-form-label'>{1}</label>".format(i, pnt_field_labels[point_type][i]) +
                "    <input type name='point_value_{0}' id='point_value_{0}' class='form-control col-lg-4'>".format(i) +
                "</div>";
    }
    $("#point_text_fields").html(text);
}


// This method cannot return the actual point values because the ajax request is asynchronous
// so, it just retrieves the point and fills the text-fields
function get_current_position(point_type)
{
    var uri = "http://{0}/robots/{1}/status".format(host, robot_id);
    var current_position = null;
    $.get(uri, { 'field': 'current_position', 'data_type': point_type }, function(data, status) {
        result = JSON.parse(data);
        if (status == "success") {
            if (result['committed']) {
                current_position = result['current_position']['value'];   // point_value = Array()

                if (current_position != null) {
                    update_current_position_text_fields(point_type, current_position);
                }
            }
        }
        if (result['committed'] == false) {
            //alert("Error in retrieving the current position!\nError msg: " + result['error_msg']);
            show_error_alert(result['error_msg'].replace('\n', '<br />'));
        }
    });
}

function init_current_position_text_fields() {
    point_type = $("#point_type").val();
    get_current_position(point_type);
}

function update_current_position_text_fields(point_type, point_value) {
    // point_value = Array()
    for (var i=0; i<pnt_field_labels[point_type].length; i++)
    {
        point_label = pnt_field_labels[point_type][i];
        $("#current_point_value_{0}".format(point_label)).val(point_value[i]);
    }
}



function get_current_position_text_fields(point_type)
{
    error = false;
    modified_current_point_value = Array()
    for (var i=0; i<pnt_field_labels[point_type].length; i++)
    {
        point_label = pnt_field_labels[point_type][i];
        value = parseFloat($("#current_point_value_{0}".format(point_label)).val());
        if (value == NaN) {
            error = true;
            break;
        }
        modified_current_point_value.push(value);
    }
    if (error)
        return null;
    return modified_current_point_value;
}

function generate_movements_control_fields(point_type)
{
    var text = "";
    for (var i=0; i<pnt_field_labels[point_type].length; i++)
    {
        point_label = pnt_field_labels[point_type][i];
        text += "<div class='form-group row'>" +
                "    <label for='{0}_plus' class='col-lg-2 col-form-label'>{1}</label>".format(i, point_label) +
                "    <button id='{0}_plus' value='{0}'  type='button' class='btn btn-default'> + </button>".format(point_label) +
                "    <input type name='current_point_value_{0}' id='current_point_value_{0}' class='form-control col-lg-4'>".format(point_label) +
                "    <button id='{0}_minus' value='{0}' type='button' class='btn btn-default'> - </button>".format(point_label) +
                "</div>";
    }
    $("#movements_control_fields").html(text);

    var uri = "http://{0}/robots/{1}/commands/move".format(host, robot_id);

    var movement_type = "";
    var step_value = 0;
    switch (point_type)
    {
        case "P": movement_type = "xyz_offset"; step_value = 20; break;
        case "J": movement_type = "joint_offset"; step_value = 4; break;
        case "T": movement_type = "tool_offset"; step_value = 1; break;
    }

    for (var i=0; i<pnt_field_labels[point_type].length; i++)
    {
        label = pnt_field_labels[point_type][i];
        $("#{0}_plus".format(label)).click(function() {
            $.post(uri, { type: movement_type, label: this.value, sign: '+', step: step_value }, function(result, status) {
                result = JSON.parse(result);
                if (status == "success") {
                    if (result['committed'] == true) {
                        show_success_alert();
                        update_current_position_text_fields(result['current_position']['data_type'], result['current_position']['value']);
                    }
                }
                if (result['committed'] == false) {
                    //alert("Error in manipulating the current position!\nResult: " + result['error_msg']);
                    show_error_alert(result['error_msg'].replace('\n', '<br />'));
                }
            });
        });

        $("#{0}_minus".format(label)).click(function() {
            $.post(uri, { type: movement_type, label: this.value, sign: '-', step: step_value }, function(result, status) {
                result = JSON.parse(result);
                if (status == "success") {
                    if (result['committed'] == true) {
                        show_success_alert();
                        update_current_position_text_fields(result['current_position']['data_type'], result['current_position']['value']);
                    }
                }
                if (result['committed'] == false) {
                    //alert("Error in manipulating the current position!\nResult: " + result['error_msg']);
                    show_error_alert(result['error_msg'].replace('\n', '<br />'));
                }
            });
        });
    }
}


function update_point_text_fields(point_name) {
    // Don't read directly from the robots, only when it is explicitly  requested
    read_point(point_name, synch=false);
}

function get_point_text_fields_values() {
// Return an Array of floats. Its size depends on the point_type of the currently selected point
    var values = Array();
    $('#point_text_fields input').each(function() {  // All <input> elements that are descendants of a #point_text_fields element
        values.push(parseFloat($(this).val()));
    });
    return values;
}


function update_connect_btn_status(connected)
{
    if (connected == null) {  // undefined/null means that it hasn't been passed as param
        var uri = "http://{0}/robots/{1}/status".format(host, robot_id);
        $.get(uri, { 'field': 'connected' }, function(data, status) {
            result = JSON.parse(data);
            if (status == "success") {
                if (result['committed']) {
                    connected = result['connected'];
                    update_connect_btn_status(connected);  // Recall itself but passing the param, so the else clause will be executed
                }
            }
        });
    }
    else {
        $("#connect_btn").val(connected ? 'disconnect' : 'connect');
        $("#connect_btn").html(connected ? 'Disconnect' : 'Connect');

        if (connected) {
            $("#motor_btn").removeAttr("disabled");
            update_motor_btn_status()
            update_speed_range();
            get_current_position($("#point_type").val());
        }
        else
            $("#motor_btn").attr('disabled', 'disabled');
    }
}

function update_motor_btn_status(motor_on)
{
    if (motor_on == null) {  // undefined/null means that it hasn't been passed as param
        var uri = "http://{0}/robots/{1}/status".format(host, robot_id);
        $.get(uri, { 'field': 'motor_on' }, function(data, status) {
            result = JSON.parse(data);
            if (status == "success") {
                if (result['committed']) {
                    motor_on = result['motor_on'];
                    update_motor_btn_status(motor_on);  // Recall itself but passing the param, so the else clause will be executed
                }
            }
        });
    }
    else {
        $("#motor_btn").val(motor_on ? 'motor_off' : 'motor_on');
        $("#motor_btn").html(motor_on ? 'Motor Off' : 'Motor On');
        $("#motor_btn").addClass(motor_on ? 'btn-danger' : 'btn-success');
        $("#motor_btn").removeClass(!motor_on ? 'btn-danger' : 'btn-success');
    }
}

function update_speed_range(speed)
{
    if (speed == null) {  // undefined/null means that it hasn't been passed as param
        var uri = "http://{0}/robots/{1}/status".format(host, robot_id);
        $.get(uri, { 'field': 'speed' }, function(data, status) {
            result = JSON.parse(data);
            if (status == "success") {
                if (result['committed']) {
                    speed = result['speed'];
                    update_speed_range(speed); // Recall itself but passing the param, so the else clause will be executed
                }
            }
            if (result['committed'] == false) {
                //alert("Error in retrieving the speed!\nError msg: " + result['error_msg']);
                show_error_alert(result['error_msg'].replace('\n', '<br />'));
            }
        });
    }
    else {
        $("#speed_range").val(speed);
        $("#speed_value_text").html(speed);
    }
}


function resetPanels()
{
    $("#robot_container").html("<h3>Robot: -</h3>" +
                               "    <div class='row'>" +
                               "        <div class='container col-lg-6'>" +
                               "            <div class='row'>" +
                               "                <div class='col-lg-4' id='robot_details_container'>" +
                               "                    <h4>Details</h4>" +
                               "                </div>" +
                               "                <div class='col-lg-8' id='robot_status_container'>" +
                               "                    <h4>Status</h4>" +
                               "                    <div class='row'>" +
                               "                        <div class='col-lg-5'><button id='connect_btn' value='disconnect' type='button' class='btn btn-info btn-md'>Connect</button></div>" + // value: "connect"/"disconnect"
                               "                        <div class='col-lg-5'><button id='motor_btn' value='motor_on' disabled='disabled' type='button' class='btn btn-success btn-md'>Motor On</button></div>" + // value: "motor_on"/"motor_off"
                               "                    </div>" +
                               "                    <div class='row'>" +
                               "                        <label for='speed_range' class='col-lg-3 col-form-label'>Speed:</label>" +
                               "                        <input type='range' id='speed_range' name='speed_range' min='1' max='100' value='1' class='form-control col-lg-6'>" +
                               "                        <div id='speed_value_text' class='col-lg-3 align-middle'>1</div>" +
                               "                    </div>" +
                               "                </div>" +
                               "            </div>" +
                               "            <div class='row'>" +
                               "                <div class='col-lg-12' id='robot_controls_container'>" +
                               "                </div>" +
                               "            </div>" +
                               "        </div>" +
                               "        <div class='container col-lg-6' id='robot_tasks_container'>" +
                               "        </div>" +
                               "    </div>");

    var robot_controls_text = "";
    robot_controls_text += "<h4>Controls</h4>" +
                           "<div class='row'>" +
                           "    <form id='points_management' action='#'>" +
                           "        <div class='form-group row'>" +
                           "            <label for='point_type' class='col-lg-3 col-form-label'>Type:</label>" +
                           "            <select name='point_type' id='point_type' class='form-control col-lg-3'>" +
                           "                <option value='P'>P</option>" +
                           "                <option value='J'>J</option>" +
                           "                <option value='T'>T</option>" +
                           "            </select>" +
                           "        </div>" +
                           "        <div class='form-group row'>" +
                           "            <label for='point_name' class='col-lg-3 col-form-label'>Point:</label>" +
                           "            <select name='point_name' id='point_name' class='form-control col-lg-3'>" +
                           "            </select>" +
                           "        </div>" +
                           "        <div id='point_text_fields'>" +
                           "        </div>" +
                           "        <button id='save_point_btn' type='button' class='btn btn-default' title='Save point on the system'>Save</button>" +
                           "        <button id='dump_point_btn' type='button' class='btn btn-default' title='Save point on the robot'>Dump</button>" +
                           "        <button id='load_point_btn' type='button' class='btn btn-default' title='Load point from the robot'>Load</button>" +
                           "        <button id='move_to_point_btn' type='button' class='btn btn-default' title='Move robot in this point'>Move To</button>" +
                           //"        <button type='reset' class='btn btn-default' title='Reset form'>Reset</button>" + // Works very badly! Better to avoid it!
                           "    <br /><br />" +
                           "    </form>" +
                           "</div>";

    robot_controls_text += "<div class='row'>" +
                           "    <form id='controls_management' action='#'>" +
                           "        <h5>Current Position</h5>" +
                           "        <div id='movements_control_fields'>" +
                           "        </div>" +
                           "        <div class='form-group row'>" +
                           "            <button id='move_current_position_btn' type='button' class='btn btn-default col-lg-2' title='Move to the new specified position'>Move</button> " +
                           "            <button id='save_curruent_point_btn' type='button' class='btn btn-default col-lg-7' title='Save current position in the specified point'>Save current position in ...</button> " +
                           //"            <label for='destination_point_name' class='col-lg-2 col-form-label'></label>" +
                           "            <select name='destination_point_name' id='destination_point_name' class='form-control col-lg-3'>" +
                           "            </select>" +
                           "        </div>" +
                           "    </form>" +
                           "</div>";

    $("#robot_controls_container").html(robot_controls_text);

    default_point_type = "P";    // P by default
    //$("#point_type").val(default_point_type).change();
    //$("#point_name:first-child").attr("selected", true).change();
    update_select_point_names(default_point_type);
    generate_point_text_fields(default_point_type);
    generate_movements_control_fields(default_point_type);

    init_tasks_dashboard();

    init_elements_action();
}


function init_elements_action()
{
    /* CONNECT/DISCONNECT BUTTON */
    $("#connect_btn").click(function() {
        var uri = "http://{0}/robots/{1}/status".format(host, robot_id);
        var connect_cmd = $(this).val();     // Get the button value, i.e. connect/disconnect
        connect_cmd = connect_cmd == 'connect';  // transofrm it in boolean

        if (connect_cmd) {
            $.post(uri, { 'field': 'connected', 'value': true }, function(result, status) {
                result = JSON.parse(result);
                if (status == "success") {
                    if (result['committed'] == true) {
                        show_success_alert("Robot \"{0}\" connected.".format(robot_id));
                        update_connect_btn_status();
                        update_speed_range();
                    }
                }
                if (result['committed'] == false)
                    //alert("Connection failed!\nError: " + result['error_msg'] + "\nStatus: " + status);
                    show_error_alert(result['error_msg'].replace('\n', '<br />'));
            });
        }
        else {
            $.post(uri, { 'field': 'connected', 'value': false }, function(result, status) {
                result = JSON.parse(result);
                if (status == "success")
                    location.reload();
                else
                    show_error_alert(result['error_msg'].replace('\n', '<br />'));
                    //("Disconnection failed!\nError: " + result['error_msg'] + "\nStatus: " + status);
            });
        }
    });

    /* MOTOR ON/OFF BUTTON */
    $("#motor_btn").click(function() {
        var uri = "http://{0}/robots/{1}/status".format(host, robot_id);
        var motor_cmd = $(this).val();  // Get the button value, i.e. motor_on/motor_off
        motor_cmd = motor_cmd == 'motor_on';  // transofrm it in boolean

        $.post("http://{0}/robots/{1}/status".format(host, robot_id), { 'field': 'motor_on', 'value': motor_cmd }, function(result, status) {
            result = JSON.parse(result);
            if (status == "success") {
                if (result['committed'] == true)  // The motor has turned on
                {
                    show_success_alert("Motor {0} on robot \"{1}\".".format(result['motor_on'] ? 'ON' : 'OFF', robot_id));
                    update_motor_btn_status(result['motor_on']);
                }
                else
                    show_error_alert(result['error_msg'].replace('\n', '<br />'));
                    //alert("Error in turning on the motor!\nError: " + ['error_msg'] + "\nStatus: " + status);
            }
            if (result['committed'] == false) {
                //alert("Error in changing motor status!\nError: " + result['error_msg'] + "\nStatus: " + status);
                show_error_alert(result['error_msg'].replace('\n', '<br />'));
            }
        });
    });





    /****************************************
    ****** CONTROLS: POINTS MANAGEMENT ******
    *****************************************/
    $("#point_type").change(function() {
        var point_type = $(this).val();
        update_select_point_names(point_type);
        generate_point_text_fields(point_type);
        generate_movements_control_fields(point_type);

        var point_name = $("#point_name").val();
        update_point_text_fields(point_name);
        init_current_position_text_fields();
    });

    $("#point_name").change(function() {
        var point_name = $(this).val();
        update_point_text_fields(point_name);
    });




    $("#save_point_btn").click(function() {
        write_point(synch=false);
    });

    $("#dump_point_btn").click(function() {
        write_point(synch=true);
    });

    $("#load_point_btn").click(function() {
        point_name = $("#point_name").val();
        read_point(point_name, synch=true);
    });

    $("#move_to_point_btn").click(function() {
        var uri = "http://{0}/robots/{1}/commands/move".format(host, robot_id);
        var point_name = $("#point_name").val();

        $.post(uri, { 'type': 'absolute_point', 'point_name': point_name }, function(result, status) {
            result = JSON.parse(result);
            if (status == "success") {
                if (result['committed'] == true) {
                    show_success_alert("Robot \"{0}\" moved in point \"{1}\".".format(robot_id, point_name));
                    init_current_position_text_fields();
                }
            }
            if (result['committed'] == false) {
                //alert("Error in moving the robot!\nResult: " + result['error_msg']);
                show_error_alert(result['error_msg'].replace('\n', '<br />'));
            }
        });
    });

    $("#move_current_position_btn").click(function() {
        var uri = "http://{0}/robots/{1}/status".format(host, robot_id);
        data_type = $("#point_type").val();
        new_current_position = get_current_position_text_fields(data_type);
        if (new_current_position == null)
            alert("Impossible to proceed!");
        else {
            $.post(uri, { 'field': 'current_position', 'value': JSON.stringify(new_current_position), 'type': data_type }, function(result, status) {
                result = JSON.parse(result);
                if (status == "success") {
                    if (result['committed'] == true) {
                        show_success_alert();
                        update_current_position_text_fields(result['current_position']['data_type'], result['current_position']['value']);
                    }
                }
                if (result['committed'] == false) {
                    //alert("Error in moving the robot!\nResult: " + result['error_msg']);
                    show_error_alert(result['error_msg'].replace('\n', '<br />'));
                }
            });
        }
    });

    $("#save_curruent_point_btn").click(function() {
        point_name = $("#destination_point_name").val();
        data_type = $("#point_type").val();
        current_position = get_current_position_text_fields(data_type);
        if (current_position == null)
            alert("Impossible to proceed!");
        else {
            var uri = "http://{0}/robots/{1}/points/{2}".format(host, robot_id, point_name);
            $.post(uri, { 'synch': false, 'value': JSON.stringify(current_position) }, function(result, status) {
                result = JSON.parse(result);
                if (status == "success") {
                    if (result['committed'] == true) {
                        if ($("#point_name").val() == point_name)
                            update_point_text_fields(point_name);
                        show_success_alert("Current position saved in point \"{0}\".".format(point_name));
                    }
                }
                if (result['committed'] == false) {
                    //alert("Error in moving the robot!\nResult: " + result['error_msg']);
                    show_error_alert(result['error_msg'].replace('\n', '<br />'));
                }
            });
        }
    });


    document.getElementById("speed_range").oninput = function() {
        $("#speed_value_text").html(this.value);
    };

    $("#speed_range").change(function () {
        speed = $(this).val();
        var uri = "http://{0}/robots/{1}/status".format(host, robot_id);
        $.post(uri, { 'field': 'speed', 'value': speed }, function(result, status) {
            if (status == "success") {
                result = JSON.parse(result);
                if (result['committed'] == true)  // The motor has turned on
                {
                    update_speed_range(result['speed']);
                    show_success_alert("Speed of robot \"{0}\" set to \"{1}\".".format(robot_id, result['speed']));
                }
            }
            if (result['committed'] == false) {
                //alert("Error in setting the new speed value!\nResult: " + result + "\nStatus: " + status);
                show_error_alert(result['error_msg'].replace('\n', '<br />'));
            }
        });
    });
}

$(document).ready(function()
{
    $("#robot_container").hide();

    $("#serialize_btn").click(function() {
        var uri = "http://{0}/serialize".format(host);
        $.post(uri, null, function(result, status) {
            result = JSON.parse(result);
            if (status == "success") {
                if (result['committed'] == true) {
                    show_success_alert("The system has been serialized.");
                }
            }
            if (result['committed'] == false)
                show_error_alert(result['error_msg'].replace('\n', '<br />'));
        });
    });

    $("#deserialize_btn").click(function() {
        var uri = "http://{0}/deserialize".format(host);
        $.post(uri, null, function(result, status) {
            result = JSON.parse(result);
            if (status == "success") {
                if (result['committed'] == true) {
                    show_success_alert("The system has been deserialized.");
                    setTimeout(function(){ location.reload(); }, 700);
                }
            }
            if (result['committed'] == false)
                show_error_alert(result['error_msg'].replace('\n', '<br />'));
        });
    });

    /* DELETE ROBOT BUTTON */
    $("#delete_robot_btn").click(function() {
        var robot_id = $(this).val();  // Get the button value, i.e. the robot_id stored in it
        $.ajax({
            url: 'http://{0}/robots/{1}'.format(host, robot_id),
            method: 'DELETE',
            contentType: 'application/json',
            success: function(result) {
                // Reset Panels and rewrite the robot lists accordingly with the current robotic cell situation
                //resetPanels();
                //...
                // Alternatively, reload the page
                location.reload();
            },
            error: function(request,msg,error) {
                // handle failure
            }
        });
    });

    /* ADD ROBOT FORM */
    $("#add_robot_form").submit(function(e) {
        e.preventDefault();
        var form = this;

        var add_robot_id = $("#add_robot_id").val();
        var add_host = $("#add_host").val();
        var add_port = $("#add_port").val();

        var check = true;

        if (check) {
            $.ajax({
                url: "http://{0}/robots/{1}".format(host, add_robot_id),
                type: 'PUT',
                data: { 'host': add_host, 'port': add_port },
                success: function(response) {
                    result = JSON.parse(response);
                    if (result['committed'] == true)
                        location.reload();
                    else
                        show_error_alert(result['error_msg'].replace('\n', '<br />'));
                        //alert("Error in adding the robot!\nResult: " + response['error_msg']);
                },
                error: function(xhr, textStatus, errorThrow) {
                    result = JSON.parse(xhr.responseText);
                    if (result['committed'] == false)
                        show_error_alert(result['error_msg'].replace('\n', '<br />'));
                        //alert(errorThrow + ":\n\nError in adding the robot!\nResult: " + result['error_msg']);
                }
            });
        }
        else
            show_error_alert("One ore more field values are invalid.");
    });

    /* MODIFY ROBOT FORM */
    $("#mdf_robot_form").submit(function(e) {
        e.preventDefault();
        var form = this;

        var uri = $(this).attr("action");
        var robot_id = $("#mdf_robot_id").val();
        var host = $("#mdf_host").val();
        var port = $("#mdf_port").val();

        var check = true;

        if (check) {
            $.post(uri, { 'robot_id': robot_id, 'host': host, 'port': port }, function(result, status) {
                result = JSON.parse(result);
                if (status == "success") {
                    if (result['committed'] == true)
                        location.reload();
                }
                if (result['committed'] == false)
                    show_error_alert(result['error_msg'].replace('\n', '<br />'));
                    //alert("Error in modifying the robot!\nResult: " + result['error_msg']);
            });
        }
        else
            show_error_alert("One ore more field values are invalid.");
    });
});


function showDetailsRobot(event, robot_id) {
    event.preventDefault();  // Prevent the link click event (so that the URL bar won't display anymore that awful #)

    window.robot_id = robot_id;
    var uri = "http://{0}/robots/{1}".format(host, robot_id);

    $.get(uri, function(data, status) {
        if (status == "success") {
            resetPanels();

            update_connect_btn_status();
            //update_motor_btn_status();

            var robot = JSON.parse(data);
            $("#robot_container").children("h3").text("Robot: " + robot['robot_id']);
            var text = "Host: " + robot['host'] + "<br />";
            text += "Port: " + robot['port'] + "<br />";
            $("#robot_details_container").html("<h4>Details</h4><div>" + text + "</div>");

            $("#modify_robot_btn").val(robot_id);
            $("#modify_robot_btn").removeAttr("disabled");
            $("#delete_robot_btn").val(robot_id);
            $("#delete_robot_btn").removeAttr("disabled");

            $("#mdf_robot_form").attr("action", uri);    // http://{host:port}/robots/robot_id
            $("#mdf_robot_form").attr("method", "POST");
            $("#mdf_robot_id").val(robot['robot_id']);
            $("#mdf_host").val(robot['host']);
            $("#mdf_port").val(robot['port']);

            point_name = $("#point_name").val();
            update_point_text_fields(point_name);

            $("#robot_container").show();
        }
    });
}


