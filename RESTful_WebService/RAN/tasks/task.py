import datetime
import logging
from threading import Thread, Event

__author__ = 'Mattia Sandrini'

class Task(Thread):
    TASK_TYPE_CUSTOM = 'custom'
    TASK_TYPE_DO_NOTHING = 'do_nothing'
    TASK_TYPE_PICK_AND_PLACE = 'pick_and_place'
    TASK_TYPES = [TASK_TYPE_CUSTOM, TASK_TYPE_DO_NOTHING, TASK_TYPE_PICK_AND_PLACE]

    STATUS_STATE_PREPARING = 0
    STATUS_STATE_EXECUTING = 1
    STATUS_STATE_DONE = 2
    STATUS_STATE_HALTED = 3

    STATUS_STATES = { STATUS_STATE_PREPARING: 'Preparing',
                      STATUS_STATE_EXECUTING: 'Executing',
                      STATUS_STATE_DONE: 'Done',
                      STATUS_STATE_HALTED: 'Halted' }


    incremental_id = 0  # This variable counts the total of Tasks generated, and it's used for creating the unique IDs
    @staticmethod
    def generate_unique_id(robot_id, task_name):
        # Note: In a multithreaded environment this method should be synchronized among the threads;
        # namely, the variable Task.incremental_id has to be accessed in mutual exclusion
        Task.incremental_id += 1
        return "{}_{}_{}".format(robot_id, task_name, Task.incremental_id)


    def __init__(self, task_name=None, robot=None, type=TASK_TYPE_CUSTOM, socketio=None, steps=None):
        assert type in Task.TASK_TYPES, "Invalid task type \"{}\".".format(type)

        self.socketio = socketio
        self.task_name = task_name
        self.robot = robot
        self.type = type
        #self.task_id = "{}_{}_{}".format(self.robot.robot_id, self.task_name, str(time.time()*100)[6:12]) # It can't assure uniqueness
        if robot != None:
            self.task_id = Task.generate_unique_id(self.robot.robot_id, self.task_name)
        else:
            self.task_id = ""
        self.steps = [] if steps is None else steps
        self.status = self.STATUS_STATE_PREPARING
        self.current_step = None
        self.progress = 0   # Float from 0 to 1
        self.measured_duration = -1  # Measure the duration of the task's execution in seconds

        self._thread_stop_event = Event()
        super(Task, self).__init__()   # Thread.init()

    def add_step(self, step):
        if step != None:
            self.steps.append(step)

    def clone(self):
        """
        Clone the task, but in a new object, with different reference pointer and a different task_id
        :return: Cloned tak with different task_id
        """
        steps_copy = []
        for s in self.steps:
            steps_copy.append(s.clone())

        return Task(task_name=self.task_name, robot=self.robot, socketio=self.socketio, steps=steps_copy)

    def execute(self):
        assert self.robot is not None, "Requires robot to be not None"

        self.status = self.STATUS_STATE_EXECUTING
        i = 0
        time_start = datetime.datetime.now()
        for step in self.steps:
            if not self._thread_stop_event.isSet():
                self.current_step = step
                logging.info("About to execute step #{} \"{}\" on robot \"{}\"...".format(i, self.current_step.name, self.robot.robot_id))

                self.current_step.execute()

                if self.socketio is not None:
                    self.progress = float((i+1)/len(self.steps))
                    logging.info("emit \"progress_msg\" form task \"{}\" progress: \"{}\"!".format(self.task_id, self.progress))
                    self.socketio.emit('progress_msg', {'task_id': self.task_id, 'progress': self.progress}, namespace='/monitor')
                i += 1
            else:
                self.halted = True
                break
        time_finish = datetime.datetime.now()
        self.measured_duration = (time_finish-time_start).seconds + (time_finish-time_start).microseconds/1000000
        self.status = self.STATUS_STATE_DONE
        self.robot.notify_task_status(self.status)
        self.socketio.emit('finish_task_msg', {'task_id': self.task_id, 'measured_duration': self.measured_duration}, namespace='/monitor')
        logging.info("Task \"{}\" Finished!".format(self.task_id))


    def run(self):
        self.execute()

    def force_stop(self):
        self._thread_stop_event.set()
        self.current_step.force_stop()
        self.status = self.STATUS_STATE_HALTED


    def serialize(self):
        return {
            'task_id': self.task_id,
            'task_name': self.task_name,
            'type': self.type,
            'steps': [s.serialize() for s in self.steps],
            'current_step': self.current_step.name if self.current_step != None else "",
            'progress': self.progress,
            'status_code': self.status,
            'status_text': self.STATUS_STATES[self.status],
            'measured_duration': self.measured_duration
        }
