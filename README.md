The programs have been coded in Python 3.6.5, using PyCharm.

Packages:
 - flask 
 - flask_socketio
 - watson_developer_cloud
 
 Linux commands:
  $ pip3 install flask
  $ pip3 install flask-socketio
  $ pip3 install --upgrade watson-developer-cloud

Run the Web Service:
 1. Execute the entry point "RESTful_WebService/main.py" 
 2. It will be listening on the port 5000
 3. Access the Web Interface by typing this URL: "http://serverIP:5000" (the interface has been tested on Chrome browser)


Run the Chatbot Service:
 4. Create an IBM bluemix account (https://console.bluemix.net/registration/)
 5. Activate the services Watson Assistant and Node-Red
    - Watson Assistant: https://console.bluemix.net/catalog/services/watson-assistant-formerly-conversation
    - Node-Red: https://console.bluemix.net/catalog/starters/node-red-starter
 6. Upload the two json files, one is the workspace of Watson Assistant, the other one of Node-Red


Setup a Telegram BOT:
 7. Create a new Telegram BOT
 8. Insert the token of the new chatBot in the python file called "Telgram.py"
 9. Update the Telegram blocks in the Node-Red flow editor with the new Token


[![Chatbot for Controlling a Robotic System](https://img.youtube.com/vi/9tuNcE8tg4s/0.jpg)](https://www.youtube.com/watch?v=9tuNcE8tg4s)

[![Distinctive approaches for H-R interactions through a Cloud Robotic based architecture](https://img.youtube.com/vi/InJsESbmgfQ/0.jpg)](https://www.youtube.com/watch?v=InJsESbmgfQ)