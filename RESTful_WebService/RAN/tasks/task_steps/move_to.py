from .task_step import TaskStep

__author__ = 'Mattia Sandrini'

class MoveTo(TaskStep):

    name = None
    parent_task = None

    def __init__(self, name="move_to", parent_task=None, point="P0", type="@0", interp="P"):
        self.point = point
        self.type = type
        self.interp = interp
        super(MoveTo, self).__init__(name, parent_task)

    def execute(self):
        if not self._thread_stop_event.isSet():
            # Do something
            #self.parent_task.notify(self)
            robot = self.parent_task.robot
            robot.move(self.point, self.type, self.interp)

    def clone(self):
        return MoveTo(name=self.name, parent_task=self.parent_task, point=self.point, type=self.type, interp=self.interp)

    def force_stop(self):
        super(MoveTo, self).force_stop()
        self.parent_task.robot.halt()   # Halt the Robot's movement


    def serialize(self):
        return {
            'name': self.name,
            'point': self.point,
            'pass_type': self.type,
            'interp': self.interp
        }
