import time
from RAN.robots.robot import Robot
import RAN

__author__ = 'Mattia Sandrini'

class Dummy(Robot):
    DEFAULT_POINT_VALUES = { 'P': [0]*6 + [-1], 'J': [0]*8, 'T': [0]*9 + [-1] }   # Denso standard

    MIN_Z_LIMIT = 80  # Before moving always check whether the position to reach is above this threshold
    INITIAL_SPEED = 1

    dummy_current_position = DEFAULT_POINT_VALUES   # Dummy current postion initialized to the default point (0,0,...,0,-1)

    def __init__(self, robot_id=None):
        super(Dummy, self).__init__(robot_id)
        self.dummy_current_position = { 'P': [280, 280, 120, -180, 0, -145, 5],
                                        'J': [280, 280, 120, -180, 0, -145, 0, 0],
                                        'T': [280, 280, 120, -180, 0, -145, 0, 0, 0, 5] }

    def init_points(self):
        super(Dummy, self).init_points()
        self.dummy_current_position = { 'P': [280, 280, 120, -180, 0, -145, 5],
                                        'J': [280, 280, 120, -180, 0, -145, 0, 0],
                                        'T': [280, 280, 120, -180, 0, -145, 0, 0, 0, 5] }

    def clone_without_tasks(self):
        copy = Dummy(self.robot_id)
        copy.connected = self.connected
        copy.motor_on = self.motor_on
        copy.speed = self.speed
        copy.points_synched = self.points_synched
        copy.points = self.points
        copy.dummy_current_position = self.dummy_current_position
        copy.delay_between_tasks = self.delay_between_tasks
        return copy

    def connect(self):
        assert not self.connected, "{} robot already connected.".format(self.robot_id)
        self.connected = True
        self.speed = self.INITIAL_SPEED
        return True

    def disconnect(self):
        super(Dummy, self).disconnect()
        return True


    def is_motor_on(self):
        return self.motor_on

    def turn_motor_on(self):
        if self.connected:
            self.motor_on = True
            return True
        else:
            return False

    def turn_motor_off(self):
        if self.connected:
            self.motor_on = False
            return True
        else:
            return False


    def take_arm(self):
        if self.connected:
            return True
        else:
            return False

    def give_arm(self):
        if self.connected:
            return True
        else:
            return False

    def move(self, point, pass_type="@0", interp="ptp"):
        if self.connected:
            assert type(point)==str and (point[0] in ['P','J','T','V']) and point[1:].isdigit(), "Point format invalid!"
            assert type(pass_type)==str and pass_type[0]=='@' and pass_type[1] in ['P','E']+[str(n) for n in range(0,10)], "Pass Type format invalid!"

            # Check to not collide the floor
            dest_point = self.get_point(point, synch=True)
            #assert dest_point[2] >= 80, "Point too close to the Z-limit"

            time.sleep(5/self.speed)   # Simulate the time in movements

            self.dummy_current_position[point[0]] = dest_point
            return True
        else:
            return False


    def move_once(self, point, interp="P"):
        if self.connected:
            pass_type = "@0"    # There are no other options because it's moving precisely in the specified point
            if self.take_arm():
                moved = self.move(point, pass_type, interp)
                self.give_arm()
                return moved
        else:
            return False

    def halt(self):
        pass


    def get_current_position(self, data_type="P"):
        if self.connected:
            data_type = data_type.upper()
            assert type(data_type)==str and data_type in self.DEFAULT_POINT_VALUES, "Data type format invalid!"

            cur_pos = self.dummy_current_position[data_type]
            return cur_pos
        else:
            return None

    def set_current_position(self, new_position, data_type="P"):
        if self.connected:
            data_type = data_type.upper()
            assert type(data_type)==str and data_type in self.DEFAULT_POINT_VALUES, "Data type format invalid!"

            self.dummy_current_position[data_type] = new_position
            return True
        else:
            return False



    def set_speed(self, speed):
        self.speed = speed
        return True

    def get_speed(self):
        return self.speed



    def load_point(self, point_name):
        point_name = point_name.upper()
        assert point_name in self.points, "Invalid point name."
        return True

    def load_points(self):
        committed = True
        for point_name in self.points:
            committed &= self.load_point(point_name)  # If just one of these loads goes wrong, then committed becomes False
        return committed

    def dump_point(self, point_name):
        point_name = point_name.upper()
        assert point_name in self.points, "Invalid point name."
        return True

#End class


#######################
# INTERESTING COMMANDS:
#
# RobInfo (Return the robot information)
#
# J2T Transform J type data to T type data. P.86
# T2J Transform T type data to J type data. P.87
# J2P Transform J type data to P type data. P.87
# P2J Transform P type data to J type data. P.88
# T2P Transform T type data to P type data. P.88
# P2T Transform P type data to T type data. P.89
#
# StartLog Start log recording. P.100
# StopLog Stop log recording. P.102
# ClearLog Clear log data. P.103
#
# MotionSkip Abort the robot motion in progress. P.112
# MotionComplete Judge whether the robot motion is complete. P.112
#
# Spline ...
#
# PayLoad Change the setting value of internal load conditions. P.133

import main

if __name__ == '__main__':
    import logging
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s:%(module)s:%(levelname)s:%(message)s')

    r = Dummy("dummy")
    r.init_points()
    r.connect()
    r.turn_motor_on()
    r.set_speed(20)
    task1 = RAN.tasks.pick_and_place.PickAndPlace(task_name="pick_and_place_1", robot=r, socketio=main.socketio)
    task2 = RAN.tasks.pick_and_place.PickAndPlace(task_name="pick_and_place_2", robot=r, socketio=main.socketio)
    task3 = RAN.tasks.pick_and_place.PickAndPlace(task_name="pick_and_place_3", robot=r, socketio=main.socketio)
    r.add_executable_task(task1)
    r.add_executable_task(task2)
    r.add_executable_task(task3)
    r.enqueue_task("pick_and_place_2")
    r.enqueue_task("pick_and_place_3")
    r.enqueue_task("pick_and_place_1")
    r.enqueue_task("pick_and_place_2")
    print(r.serialize()['tasks_queue'][0]['task_id'])
    print(r.serialize()['tasks_queue'][3]['task_id'])

    #print(r.serialize())
    #is_task_executed = r.execute_next_task()
    #print("is_task_executed: " + str(is_task_executed))

    is_task_executed = r.execute_next_task()
    print("is_task_executed: " + str(is_task_executed))

    #time.sleep(5)
    #is_task_executed = r.execute_next_task()
    #print("is_task_executed: " + str(is_task_executed))

    r.turn_motor_off()
    r.disconnect()
