from RAN.tasks.task import Task
from RAN.tasks.task_steps.move_to import MoveTo
from RAN.tasks.task_steps.take_arm import TakeArm
from RAN.tasks.task_steps.give_arm import GiveArm
from RAN.tasks.task_steps.wait import Wait

__author__ = 'Mattia Sandrini'


class PickAndPlace(Task):

    def _generate_example_steps(self, repeat_times, delay_between_repetitions):
        self.steps = [TakeArm("Take Arm", self)]
        for i in range(repeat_times+1):
            self.steps += [MoveTo("MoveTo P0", self, "P0", "@0"),
                           MoveTo("MoveTo P1", self, "P1", "@P"),
                           MoveTo("MoveTo P2", self, "P2", "@P"),
                           MoveTo("MoveTo P3", self, "P3", "@0"),
                           MoveTo("MoveTo P2", self, "P2", "@P"),
                           MoveTo("MoveTo P1", self, "P1", "@P"),
                           MoveTo("MoveTo P0", self, "P0", "@0")]
            if i < repeat_times:  # Don't wait at the last repetition end
                self.steps += [Wait("Wait", self, delay_between_repetitions)]
        # End For
        self.steps += [GiveArm("Give Arm", self)]


    def _generate_steps(self, points, repeat_times, delay_between_repetitions):
        self.steps = [TakeArm("Take Arm", self)]
        repeat_times = 1 if repeat_times <= 0 else repeat_times
        for i in range(repeat_times):
            for pnp_point in points:
                self.steps += [MoveTo("MoveTo pick_approach "+pnp_point['pick_approach'], self, pnp_point['pick_approach'], "@P"),
                               MoveTo("MoveTo pick "+pnp_point['pick'], self, pnp_point['pick'], "@0"),
                               MoveTo("MoveTo pick_approach "+pnp_point['pick_approach'], self, pnp_point['pick_approach'], "@P"),
                               MoveTo("MoveTo place_approach "+pnp_point['place_approach'], self, pnp_point['place_approach'], "@P"),
                               MoveTo("MoveTo place "+pnp_point['place'], self, pnp_point['place'], "@0"),
                               MoveTo("MoveTo place_approach "+pnp_point['place_approach'], self, pnp_point['place_approach'], "@P")]
            if i < repeat_times:    # Don't wait at the last repetition end
                self.steps += [Wait("Wait", self, delay_between_repetitions)]
        # End For
        self.steps += [GiveArm("Give Arm", self)]

    def __init__(self, task_name=None, robot=None, socketio=None, steps=None, points=None, repeat_times=0, delay_between_repetitions=0):
        """

        :param task_name:
        :param robot:
        :param socketio:
        :param steps:
        :param points: list of dicts formatted in this way: [{'pick_approach': 'P0', 'pick': 'P1', 'place_approach': 'P2', 'place': 'P3'}, {...}, ...]
        :param repeat_times: if 0 the task is executed only once
        """
        self.points = points
        self.repeat_times = repeat_times
        if steps is None and points is None:
            self._generate_example_steps(self.repeat_times, delay_between_repetitions)
        elif steps is None and points != None:
            self._generate_steps(self.points, self.repeat_times, delay_between_repetitions)
        elif steps != None:
            self.steps = steps

        super(PickAndPlace, self).__init__(task_name, robot, Task.TASK_TYPE_PICK_AND_PLACE, socketio, self.steps)
