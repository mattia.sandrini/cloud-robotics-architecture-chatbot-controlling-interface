# author: Emilio Baresi

import time
import requests
import conversation

#Sending an http request as input for the Watson Conversation Services
def sendMessageToConversation(msg,contesto):
    url = "https://robotic-app-cloudant.eu-gb.mybluemix.net/inputConversation/"
    if contesto == "":
        r = requests.post(url, data = {'pass': "password", 'msg': msg, 'first': 1})
    else:
        r = requests.post(url, data = {'contesto':contesto, 'pass': "password", 'msg': msg, 'first': 0})
    return r

#Hub to handle the different intents
def handleIntent(intent, entity, output, context):
    ret = ""

    #Intent that trigger the system
    if intent == "helloSystem":
        ret = ret + output + "\n"
        return context, ret

    #Intent asking for the robot list
    if intent == "robotList" and context['auth'] == 1:
        ret = ret + output + "\n"
        #The robot list is obtained throug an GET request
        url = "http://127.0.0.1:5000/robots"
        r = requests.get(url, params=None)
        r_json = r.json()
        robots = r_json['robots']

        #Add the robots to the entity robots in conversation
        conversation.createRobotList(robots)

        if(len(robots) == 0):
            ret = ret + "The list is empty \n"
        for i in range(0,len(robots)):
            ret = ret + robots[i]['robot_id'] + "\n"
        return context, ret

    #Intent to connect to a specific robot
    if intent == "connect" and context['auth'] == 1:
        ret = ret + output + "\n"
        time.sleep(1)
        #the id of the robot is an entity contained in the user message
        id_robot = entity['value']
        #The connection is done through a get request
        url = "http://127.0.0.1:5000/robots/"+id_robot+"/status"
        params_get = {'field': 'connected'}
        data_post = {'field': 'connected', 'value': 'true'}
        r = requests.post(url, data=data_post)
        try:
            rjson = r.json()
            resultConnection = rjson['connected']
        except Exception:
            print(r.text)

        if resultConnection:
            ret = ret + "The connection Success" + "\n"
            context['connection'] = 1
            getListOfTasks(context['robot'])
        else:
            ret = ret + "The connection Failed" + "\n"
            context['connection'] = 0

        context['robot_id'] = id_robot
        return context, ret
    #Intent to disconnect from the robot
    if intent == "disconnect":
        ret = ret + output + "\n"
        url = "http://127.0.0.1:5000/robots/" + context['robot'] + "/status"
        params_get = {'field': 'connected'}
        data_post = {'field': 'connected', 'value': 'false'}
        r = requests.post(url, data=data_post)
        rjson = r.json()
        return context, ret


    if intent == "motoron":
        ret = ret + output + "\n"
        url = "http://127.0.0.1:5000/robots/" + context['robot'] + "/status"
        params_get = {'field': 'motor_on'}
        data_post = {'field': 'motor_on', 'value': 'true'}
        r = requests.post(url, data=data_post)
        return context, ret


    if intent == "listTask":
        ret = ret + output + "\n"
        tasks = getListOfTasks(context['robot'])
        for i in range(0,len(tasks)):
            ret+= "  - " + tasks[i]['task_name'] + "\n"
        return context, ret

    if intent == "execute":
        ret = ret + output + "\n"
        #Request the list of task and check if the id in Entitie exist
        print(entity)
        task = entity['value']
        url = "http://127.0.0.1:5000/robots/" + context['robot'] + "/tasks/running_queue"
        params_post = {'action': 'enqueue_task', 'task_name': task}
        r = requests.post(url, params = params_post)
        params_post = {'action': 'execute_all', 'delay': 0}
        r = requests.post(url, params=params_post)
        r_json = r.json()
        context['task'] = entity['value']
        return context, ret

    if intent == "addRobot":
        ret = ret + output + "\n"
        return context, ret

    try:
        if context["add"]==1:
            robot_id = context["new_robot"]
            robot_ip = context["new_ip"]
            robot_port = context['new_port']
            ################ Request to add a new Robot #######################
            url = "http://127.0.0.1:5000/robots/" + robot_id
            data_put = {'host': robot_ip, 'port': robot_port}
            r = requests.put(url, data=data_put)

            ###################################################################

            ret = ret + output + "\n"
            context["add"] = 0
            return context, ret
    except Exception:
        pass

    try:
        if context["new_point"] == 1:
            new_name = context["new_name"]
            new_x = context['new_x']
            new_y = context['new_y']
            new_z = context['new_z']
            #Retrieving the pose of the robot
            url = "http://127.0.0.1:5000/robots/" + context['robot_id'] + "/status"
            data_get = {'field': 'current_position', 'data_type': 'P'}
            r = requests.get(url, data=data_get)
            rjson = r.json()

            if rjson['committed'] == False:
                #If retrieve the POSE of the robot is imbossible than the pose is set as default values
                Rx = -180
                Ry = 0
                Rz = 150
                Comp = 1
            else:
                Rx = rjson['current_position']['value'][3]
                Ry = rjson['current_position']['value'][4]
                Rz = rjson['current_position']['value'][5]
                Comp = rjson['current_position']['value'][6]

            url = "http://127.0.0.1:5000/robots/" + context['robot_id'] + "/points/" + new_name
            data_post = {'synch': True, 'value': '['+new_x+','+new_y+','+new_z+','+Rx+','+Ry+','+Rz+','+Comp+']'}
            r = requests.post(url, data=data_post)
            ret = ret + output + "\n"
            context["new_point"] = 0
            return context, ret
    except Exception:
        pass


    if 'move' in context and context['move'] == 1:
        point = context['point_to_move']

        url = "http://127.0.0.1:5000/robots/" + context['robot_id'] + "/status"
        data_post = {'field': 'motor_on', 'value': 'true'}
        r = requests.post(url, data=data_post)

        url = "http://127.0.0.1:5000/robots/" + context['robot_id'] + "/commands/move"
        data_post = {'type': 'absolute_point', 'point_name': point}
        r = requests.post(url, data=data_post)

        url = "http://127.0.0.1:5000/robots/" + context['robot_id'] + "/status"
        data_post = {'field': 'motor_on', 'value': 'false'}
        r = requests.post(url, data=data_post)

        context['move'] = 0
        ret = ret + output + "\n"
        return context, ret


    if 'move_x' in context and context['move_x'] == 1:
        offset = context['x_offset']
        direction = context['direction']
        motor_on(context['robot_id'])
        url = "http://127.0.0.1:5000/robots/" + context['robot_id'] + "/commands/move"
        data_post = {'type': 'xyz_offset', 'label': 'X', 'step': offset, 'sign': direction}
        r = requests.post(url, data=data_post)
        motor_off(context['robot_id'])
        context['move_x'] = 0
        ret = ret + output + "\n"
        return context, ret



    if 'move_y' in context and context['move_y'] == 1:
        offset = context['y_offset']
        direction = context['direction']
        motor_on(context['robot_id'])
        url = "http://127.0.0.1:5000/robots/" + context['robot_id'] + "/commands/move"
        data_post = {'type': 'xyz_offset', 'label': 'Y', 'step': offset, 'sign': direction}
        r = requests.post(url, data=data_post)
        motor_off(context['robot_id'])
        context['move_y'] = 0
        ret = ret + output + "\n"
        return context, ret


    if 'move_z' in context and context['move_z'] == 1:
        offset = context['z_offset']
        direction = context['direction']
        motor_on(context['robot_id'])
        url = "http://127.0.0.1:5000/robots/" + context['robot_id'] + "/commands/move"
        data_post = {'type': 'xyz_offset', 'label': 'Z', 'step': offset, 'sign': direction}
        r = requests.post(url, data=data_post)
        motor_off(context['robot_id'])
        context['move_z'] = 0
        ret = ret + output + "\n"
        return context, ret

    if 'save' in context and context['save'] == 1:
        variable = context['variable']
        url = "http://127.0.0.1:5000/robots/" + context['robot_id'] + "/status"
        data_get = {'field': 'current_position', 'data_type': 'P'}
        r = requests.get(url, data=data_get)
        rjson = r.json()

        if rjson['committed'] == False:
            ret = ret + "Some problem during the gathering of the robot position occourred \n"
        else:
            x = rjson['current_position']['value'][0]
            y = rjson['current_position']['value'][1]
            z = rjson['current_position']['value'][2]
            Rx = rjson['current_position']['value'][3]
            Ry = rjson['current_position']['value'][4]
            Rz = rjson['current_position']['value'][5]
            Comp = rjson['current_position']['value'][6]

        url = "http://127.0.0.1:5000/robots/" + context['robot_id'] + "/points/" + variable
        data_post = {'synch': True, 'value': '[{},{},{},{},{},{},{}]'.format(x,y,z,Rx,Ry,Rz,Comp)}
        r = requests.post(url, data=data_post)

        context['save'] = 0
        ret = ret + output + "\n"
        return context, ret


    if "setSpeed" in context and context['setSpeed'] == 1:
        speed = context['speed']

        url = "http://127.0.0.1:5000/robots/" + context['robot'] + "/status"
        data_post = {'field': 'speed', 'value': speed}
        r = requests.post(url, data=data_post)

        context['setSpeed'] = 0
        ret = ret + output + "\n"
        return context, ret



    if True:
        context['auth'] = 1
        ret = ret + output + "\n"
        return context, ret



def motor_on(robot_id):
    url = "http://127.0.0.1:5000/robots/" + robot_id + "/status"
    data_post = {'field': 'motor_on', 'value': 'true'}
    r = requests.post(url, data=data_post)

def motor_off(robot_id):
    url = "http://127.0.0.1:5000/robots/" + robot_id + "/status"
    data_post = {'field': 'motor_on', 'value': 'false'}
    r = requests.post(url, data=data_post)

def backDoor(contesto):
    contesto['auth'] = 1
    return contesto

def out(msg):
    print(msg)

def getListOfTasks(rob):
    # Request the list of tasks
    url = "http://127.0.0.1:5000/robots/" + rob + "/tasks/executables"
    params_get = {}
    r = requests.get(url, data=None)
    r_json = r.json()
    tasks = r_json['executable_tasks']
    conversation.createTasksList(tasks)
    return tasks
