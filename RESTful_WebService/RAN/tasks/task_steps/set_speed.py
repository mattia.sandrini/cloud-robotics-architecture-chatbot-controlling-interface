from .task_step import TaskStep

__author__ = 'Mattia Sandrini'

class SetSpeed(TaskStep):

    def __init__(self, name="set_speed", parent_task=None, speed=1):
        self.value = speed
        super(SetSpeed, self).__init__(name, parent_task)

    def execute(self):
        if not self._thread_stop_event.isSet():
            self.parent_task.robot.set_speed(self.value)

    def clone(self):
        return SetSpeed(name=self.name, parent_task=self.parent_task, speed=self.value)

    def serialize(self):
        return {
            'name': self.name,
            'value': self.value
        }
