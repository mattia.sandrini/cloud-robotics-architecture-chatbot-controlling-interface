# author: Mattia Sandrini 

#!/usr/bin/env python3

# Application Entry Point
# NB: if the server is listening on the port 80, this file must be execute as admin
import logging
from RAN.robots.denso import Denso
from RAN.robots.dummy import Dummy
from RAN.robotic_cell import RoboticCell
import request_dispatcher

__author__ = 'Mattia Sandrini'

app = request_dispatcher.app
socketio = request_dispatcher.socketio
#app.config['SECRET_KEY'] = 'secret!'
app.config['DEBUG'] = True

#logging.basicConfig(filename='server.log', level=logging.DEBUG, format='%(asctime)s:%(module)s:%(levelname)s:%(message)s')
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s:%(module)s:%(levelname)s:%(message)s')


if __name__ == '__main__':

    cell = RoboticCell()  # Will contain the whole robotic_cell structure
    deserialization_success = cell.deserialize_xml(request_dispatcher.FILE_XML)
    if not deserialization_success or cell.robots == []:
        print("Generating example entities...")
        r1 = Denso("VS-060", "169.254.165.100", 5007)
        r1.init_points()
        from RAN.tasks.pick_and_place import PickAndPlace
        #task1 = PickAndPlace(task_name="PnP_A", robot=r1, socketio=socketio)
        #r1.add_executable_task(task1)
        #r1.enqueue_task("PnP_A")

        r2 = Dummy("dummy")
        r2.init_points()

        task1 = PickAndPlace(task_name="PnP_A", robot=r2, socketio=socketio)
        task2 = PickAndPlace(task_name="PnP_B", robot=r2, socketio=socketio)
        task3 = PickAndPlace(task_name="PnP_C", robot=r2, socketio=socketio)
        r2.add_executable_task(task1)
        r2.add_executable_task(task2)
        r2.add_executable_task(task3)

        cell = RoboticCell([r1, r2])
        if cell.serialize_xml(request_dispatcher.FILE_XML):
           print("Success in serializing on file!")

    request_dispatcher.cell = cell

    socketio.run(app, host='0.0.0.0', port=5000)   # 0.0.0.0 means that the server is listening on all the IP addressed of this machine


# EXECUTABLE COMMANDS FROM ANOTHER PYTHON SCRIPT:
#import requests
#r = requests.post("http://localhost:5000/robot_id", data={'robot_id': 101, 'var_a': 'ciao'})
#r = requests.post("http://localhost:5000/shutdown")
#r = requests.post("http://localhost:5000/robots/antropomorfo/status", data={'field':'connected', 'value': 'true'})
#r.content
#r.json()['committed'] == True
