
String.prototype.format = function() {
    var a = this;
    for (var k in arguments) {
        a = a.replace(new RegExp("\\{" + k + "\\}", 'g'), arguments[k]);
    }
    return a;
}

function show_success_alert(msg) {
    if (msg == null || msg == "")
        msg = "Operation carried out correctly.";
    msg = "<strong>Success: </strong>" + msg;

    text = "<button type='button' class='close' data-dismiss='alert'>x</button>" +
           "<div>"+ msg +"</div>";
    div_alert = $("<div class='alert alert-success'></div>").html(text);
    div_alert.fadeTo(2000, 500).slideUp(500, function(){
        $(this).slideUp(500);
    });
    $("#alerts_container").prepend(div_alert);
}

function show_error_alert(msg) {
    if (msg == null || msg == "")
        msg = "A problem occurred.";
    msg = "<strong>Error: </strong>" + msg;

    text = "<button type='button' class='close' data-dismiss='alert'>x</button>" +
           "<div>"+ msg +"</div>";
    div_alert = $("<div class='alert alert-danger'></div>").html(text);
    /*div_alert.fadeTo(2000, 500).slideUp(500, function(){
        $(this).slideUp(500);
    });*/
    $("#alerts_container").prepend(div_alert);
}
