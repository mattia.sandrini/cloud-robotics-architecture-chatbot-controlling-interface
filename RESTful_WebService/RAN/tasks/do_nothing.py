from RAN.tasks.task import Task
from RAN.tasks.task_steps.task_step import TaskStep
from RAN.tasks.task_steps.wait import Wait

__author__ = 'Mattia Sandrini'


class DoNothing(Task):

    def _generate_steps(self, repeat_times, delay_between_repetitions):
        self.steps = []
        for i in range(repeat_times):
            self.steps += [TaskStep("nop", self)]
            if i < repeat_times-1:
                self.steps += [Wait("Wait", self, delay_between_repetitions)]
        # End For

    def __init__(self, task_name=None, robot=None, socketio=None, steps=None, repeat_times=1):
        self.repeat_times = repeat_times
        if steps is None:
            self._generate_steps(self.repeat_times, delay_between_repetitions=1000)
        else:
            self.steps = steps

        super(DoNothing, self).__init__(task_name, robot, Task.TASK_TYPE_DO_NOTHING, socketio, self.steps)
