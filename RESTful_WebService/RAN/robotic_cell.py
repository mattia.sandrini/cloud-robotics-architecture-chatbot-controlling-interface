import xml_serializer, xml_deserializer
from RAN.tasks.task import Task

__author__ = 'Mattia Sandrini'

class RoboticCell():

    def __init__(self, robots=[]):
        self.robots = robots

    def search_robot(self, robot_id):
        """
        :param robot_id:
        :return: The searched robot instance if found, None otherwise
        """
        for robot in self.robots:
            if robot.robot_id == str(robot_id):
                return robot
        return None


    def delete_robot(self, robot_id):
        """
        :param robot_id:
        :return: True if the robot has been found and deleted, False otherwise
        """
        robot = self.search_robot(robot_id)
        if robot != None:
            self.robots.remove(robot)
            del robot
            return True
        else:
            return False


    def serialize(self):
        return {
            'robots': [r.serialize() for r in self.robots]
           #'sensors': [s.serialize() for s in self.sensors]
           #'cameras': [c.serialize() for c in self.cameras]
        }

    ## Serializza tutte le informazioni mantenute nella struttura di contenimento (partendo dalla lista di buildings) in un file XML
    #  @param file_xml Nome del file in cui scrivere le informazioni
    def serialize_xml(self, file_xml):
        try:
            xml_serializer.serialize(self.robots, 'robots', ['task_id'], file_xml)
            return True
        except Exception as e:
            return False

    def restore_references(self):
        import request_dispatcher
        max_task_incremental_id = 0
        for robot in self.robots:
            for task in robot.executable_tasks + robot.tasks_queue:  # Concatenate the task lists in the current robot
                task.robot = robot
                task.socketio = request_dispatcher.socketio
                for step in task.steps:
                    step.parent_task = task

                if max_task_incremental_id < task.incremental_id:
                    max_task_incremental_id = task.incremental_id
        Task.incremental_id = max_task_incremental_id

    ## Deserializza  le informazioni contenute nel file XML specificato, e le pone nella struttura di contenimento rappresentata dalla lista di buildings
    #  @param file_xml Nome del file da cui leggere le informazioni
    def deserialize_xml(self, file_xml):
        robot_list = xml_deserializer.deserialize(file_xml)
        if robot_list == None:
            self.robots = []
            return False
        else:
            self.robots = robot_list
            self.restore_references()  # Restore the parental references for tasks and task_steps
            return True
