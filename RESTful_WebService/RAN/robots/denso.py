import logging
from RAN.pybcapclient import bcapclient
from RAN.robots.robot import Robot


__author__ = 'Mattia Sandrini'

class Denso(Robot):
    TEMPORARY_POINT_P = "P99"  # Point used for modifying the robot's current position
    TEMPORARY_POINT_J = "J99"  # Point used for modifying the robot's current position
    TEMPORARY_POINT_T = "T99"  # Point used for modifying the robot's current position
    TEMPORARY_POINT = { 'P': TEMPORARY_POINT_P, 'J': TEMPORARY_POINT_J, 'T': TEMPORARY_POINT_T }

    DEFAULT_POINT_VALUES = { 'P': [0]*6 + [-1], 'J': [0]*8, 'T': [0]*9 + [-1] }

    MIN_Z_LIMIT = 80  # Before moving always check whether the position to reach is above this threshold
    INITIAL_SPEED = 3 # For security reasons always set speed to a low value as soon as the system connect to the robot

    ### Generic attributes
    host = None
    port = None

    ### Handl
    m_bcap=None
    h_ctrl=None
    h_robot=None

    ### Set param
    Name = ""
    Provider="CaoProv.DENSO.VRC"
    Machine = "localhost"
    Option = ""

    timeout = 2000


    def __init__(self, robot_id=None, host=None, port=None):
        super(Denso, self).__init__(robot_id)
        self.host = host
        self.port = port


    def _reset(self):
        self.m_bcap = None
        self.h_robot = None
        self.h_ctrl = None
        self.connected = False
        self.motor_on = False
        self.points_synched = False

    def connect(self):
        if not self.connected:
            try:
                self.m_bcap = bcapclient.BCAPClient(self.host, self.port, self.timeout)
                self.m_bcap.service_start("")
                ### Connect to RC8 (RC8(VRC)provider)
                self.h_ctrl = self.m_bcap.controller_connect(self.Name, self.Provider, self.Machine, self.Option)
                ### get Robot Object Handl
                self.h_robot = self.m_bcap.controller_getrobot(self.h_ctrl, "", "")
                # For security reasons always set speed to a low value as soon as the system connect to the robot
                self.set_speed(self.INITIAL_SPEED)
                self.connected = True
                logging.info("Robot \"{}\" connected on {}:{}.".format(self.robot_id, self.host, self.port))
                return True
            except Exception as e:
                logging.warning("Error in connecting the robot \"{}\"".format(self.robot_id))
                print(e)
                self._reset()
                return False
        else:
            return True

    def disconnect(self):
        try:
            if self.h_robot != 0:
                self.m_bcap.robot_release(self.h_robot)
            if self.h_ctrl != 0:
                self.m_bcap.controller_disconnect(self.h_ctrl)
            self.m_bcap.service_stop()
            logging.info("Robot \"{}\" disconnected.".format(self.robot_id))
        except Exception as e:
            pass
        self._reset()

    def read_variable(self, var_name):
        try:
            var_handler = self.m_bcap.controller_getvariable(handle=self.h_ctrl, name=var_name, option="")
            result = self.m_bcap.variable_getvalue(handle=var_handler)
            self.m_bcap.variable_release(var_handler)
            logging.info("\"{}\" reading variable \"{}\": {}.".format(self.robot_id, var_name, result))
            return result
        except Exception as e:
            logging.warning("\"{}\": Error in reading variable \"{}\".".format(self.robot_id, var_name))
            logging.warning(e)
            return None

    def is_motor_on(self):
        value = self.read_variable("@SERVO_ON")
        if value != None:
            self.motor_on = value
            logging.info("\"{}\" motor status: {}.".format(self.robot_id, 'on' if self.motor_on else 'off'))
            return self.motor_on
        else:
            logging.warning("\"{}\": Error in reading motor status.".format(self.robot_id))
            return None

    def turn_motor_on(self):
        if self.connected:
            try:
                self.m_bcap.robot_execute(handle=self.h_robot, command="Motor", param=True)
                result = self.is_motor_on()
                if result is None:
                    self.motor_on = False
                    return False
                logging.info("\"{}\": Motor On".format(self.robot_id))
                return True
            except Exception as e:
                logging.warning("\"{}\": Error in turning on the motor.".format(self.robot_id))
                logging.warning(e)
                return False
        else:
            return False

    def turn_motor_off(self):
        if self.connected:
            try:
                Command = "Motor"
                self.m_bcap.robot_execute(self.h_robot, Command, False)
                self.motor_on = False
                logging.info("\"{}\": Motor Off".format(self.robot_id))
                return True
            except Exception as e:
                logging.warning("\"{}\": Error in turning on the motor.".format(self.robot_id))
                logging.warning(e)
                return False
        else:
            return False


    def take_arm(self):
        try:
            self.m_bcap.robot_execute(handle=self.h_robot, command="TakeArm", param=[0,0])
            logging.info("\"{}\": Take Arm".format(self.robot_id))
            return True
        except:
            logging.warning("\"{}\": Take Arm failed.".format(self.robot_id))
            return False

    def give_arm(self):
        try:
            self.m_bcap.robot_execute(handle=self.h_robot, command="GiveArm", param=None)
            logging.info("\"{}\": Give Arm".format(self.robot_id))
            logging.warning("\"{}\": Give Arm failed.".format(self.robot_id))
            return True
        except:
            return False

    def convert_cmd(self, command, pose):
        try:
            result = self.m_bcap.robot_execute(handle=self.h_robot, command=command, param=pose)
            return result
        except Exception as e:
            print("Error in converting {}.".format(command))
            return None


    def move(self, point, pass_type="@0", interp="P"):
        """

        :param point:
        :param pass_type:
        :return: True if there hasn't been a error during the movement, False otherwise
        """
        if self.connected:
            # Check the format of point: e.g. "P13" is correct
            assert type(point)==str and (point[0] in ['P','J','T','V']) and point[1:].isdigit(), "Point format invalid!"
            # Check the format of pass_type: e.g. "@P", "@E", "@0", "@3" are correct
            assert type(pass_type)==str and pass_type[0]=='@' and pass_type[1] in ['P','E']+[str(n) for n in range(0,10)], "Pass Type format invalid!"
            # Check the interpolation type
            assert interp in ['P', 'L', 'A', 'S'], "Interpolation parameter is invalid!"

            # Check to not collide the floor
            dest_point = self.get_point(point, synch=True)
            if point[0] == 'P':
                z = dest_point[2]
            elif point[0] == 'J':
                z = self.convert_cmd('J2P', point)[2]
            elif point[0] == 'T':
                z = self.convert_cmd('T2P', point)[2]

            assert z >= 80, "Point too close to the Z-limit"

            pose = "{} {}". format(pass_type, point)
            try:
                comp_value = ['P', 'L', 'A', 'S'].index(interp) + 1
                self.m_bcap.robot_move(handle=self.h_robot, comp=comp_value, pose=pose, option="")
                logging.info("\"{}\": movement \"{}\" succeeded".format(self.robot_id, pose))
                return True
            except Exception as e:
                logging.warning("\"{}\": movement \"{}\" failed".format(self.robot_id, pose))
                logging.warning(e)
                return False
        else:
            return False


    def move_once(self, point, interp="ptp"):
        return super(Denso, self).move_once(point=point)

    def halt(self):
        self.m_bcap.robot_halt(handle=self.h_robot, option="")


    def get_current_position(self, data_type="P"):
        """
        :param data_type: "P" for retrieving the current position by P type data, "J" by J type data, "T" by T type data
        :return: 7-values array if data_type is "P", 8-values array if "J", 10-values array if "T", None if it's impossible to retrieve the current position
        """
        if self.connected:
            type2cmd = { 'P': "CurPos", 'J': "CurJnt", 'T': "CurTrn" }
            data_type = data_type.upper()
            assert type(data_type)==str and data_type in type2cmd, "Data type format invalid!"

            try:
                cur_pos = self.m_bcap.robot_execute(handle=self.h_robot, command=type2cmd[data_type], param="")
                logging.info("\"{}\": current position retrieved: \"{}\"".format(self.robot_id, str(cur_pos)))
                return cur_pos
            except Exception as e:
                logging.warning("\"{}\": get current position failed".format(self.robot_id))
                logging.warning(e)
                return None
        else:
            return None

    def set_current_position(self, new_position, data_type="P"):
        """

        :param new_position:
        :param data_type:
        :return:
        """
        if self.connected:
            data_type = data_type.upper()
            assert type(data_type)==str and data_type in self.TEMPORARY_POINT, "Data type format invalid!"

            #try:
                #var_handler = self.m_bcap.controller_getvariable(handle=self.h_ctrl, name=self.TEMPORARY_POINT[data_type], option="")
                #self.m_bcap.variable_putvalue(handle=var_handler, newval=new_position)
                #self.m_bcap.variable_release(var_handler)
            #except:
                #return False
            if super(Denso, self).set_point(point_name=self.TEMPORARY_POINT[data_type], new_value=new_position, synch=True):
                return self.move_once(point=self.TEMPORARY_POINT[data_type])
            else:
                return False
        else:
            return False

    def move_xyz_offset(self, x=0, y=0, z=0, rx=0, ry=0, rz=0, coords=None):
        return super(Denso, self).move_xyz_offset(x, y, z, rx, ry, rz, coords)

    def move_joint_offset(self, j1=0, j2=0, j3=0, j4=0, j5=0, j6=0, j7=0, j8=0, joints=None):
        return super(Denso, self).move_joint_offset(j1, j2, j3, j4, j5, j6, j7, j8, joints)

    def move_tool_offset(self, x=0, y=0, z=0, ox=0, oy=0, oz=0, ax=0, ay=0, az=0, coords=None):
        pass


    def set_speed(self, speed):
        try:
            self.m_bcap.robot_execute(handle=self.h_robot, command="ExtSpeed", param=speed)
            self.speed = speed
            logging.info("\"{}\": Speed set to: \"{}\"".format(self.robot_id, str(speed)))
            return True
        except Exception as e:
            logging.warning("\"{}\": speed setting failed.".format(self.robot_id))
            logging.warning(e)
            return False

    def get_speed(self):
        #value = self.read_variable("@EXTSPEED") # It doesn't work!
        #value = self.m_bcap.robot_execute(handle=self.h_robot, command="CurSpd", param="") # It doesn't work!
        #print("SPEED: " + str(value))
        value = self.speed
        if value != None:
            self.speed = value
            logging.info("\"{}\" speed value: {}.".format(self.robot_id, value))
            return self.speed
        else:
            logging.warning("\"{}\": Error in reading speed value.".format(self.robot_id))
            return None


    def init_points(self):
        return super(Denso, self).init_points()

    def load_point(self, point_name):
        point_name = point_name.upper()
        assert point_name in self.points, "Invalid point name."
        try:
            var_handler = self.m_bcap.controller_getvariable(handle=self.h_ctrl, name=point_name, option="")
            self.points[point_name] = self.m_bcap.variable_getvalue(handle=var_handler)
            self.m_bcap.variable_release(var_handler)
            logging.info("\"{0}\": point \"{1}\" loaded into the system: \"{1}\" = {2}".format(self.robot_id, str(point_name), str(self.points[point_name])))
        except Exception as e:
            logging.warning("\"{}\": point \"{}\" loading failed.".format(self.robot_id, point_name))
            logging.warning(e)
            return False
        return True

    def load_points(self):
        return super(Denso, self).load_points()

    def dump_point(self, point_name):
        point_name = point_name.upper()
        assert point_name in self.points, "Invalid point name."
        try:
            var_handler = self.m_bcap.controller_getvariable(handle=self.h_ctrl, name=point_name, option="")
            self.m_bcap.variable_putvalue(handle=var_handler, newval=self.points[point_name])
            self.m_bcap.variable_release(var_handler)
            logging.info("\"{0}\": point \"{1}\" dumped into the robot controller: \"{1}\" = {2}".format(self.robot_id, str(point_name), str(self.points[point_name])))
        except Exception as e:
            logging.warning("\"{}\": point \"{}\" dumping failed.".format(self.robot_id, point_name))
            logging.warning(e)
            return False
        return True

    def dump_points(self):
        return super(Denso, self).dump_points()

    def set_point(self, point_name, new_value, synch=False):
        return super(Denso, self).set_point(point_name, new_value, synch)

    def get_point(self, point_name, synch=False):
        return super(Denso, self).get_point(point_name, synch)

    def delete_point(self, point_name, synch=False):
        return super(Denso, self).delete_point(point_name, synch)


    def get_running_task(self):
        return super(Denso, self).get_running_task()


    def serialize(self):
        result = super(Denso, self).serialize()
        result.update({
            'host': self.host,
            'port': self.port
        })
        return result

#End class


#######################
# INTERESTING COMMANDS:
#
# RobInfo (Return the robot information)
#
# J2T Transform J type data to T type data. P.86
# T2J Transform T type data to J type data. P.87
# J2P Transform J type data to P type data. P.87
# P2J Transform P type data to J type data. P.88
# T2P Transform T type data to P type data. P.88
# P2T Transform P type data to T type data. P.89
#
# StartLog Start log recording. P.100
# StopLog Stop log recording. P.102
# ClearLog Clear log data. P.103
#
# MotionSkip Abort the robot motion in progress. P.112
# MotionComplete Judge whether the robot motion is complete. P.112
#
# Spline ...
#
# PayLoad Change the setting value of internal load conditions. P.133


if __name__ == "__main__":
    r = Denso("antropomorfo", "169.254.165.133", 5007)
    r.init_points()

    r.connect()
    #print(r.get_speed())
    print("J2P of J0: " + str(r.j2p("J0")))
    r.disconnect()


